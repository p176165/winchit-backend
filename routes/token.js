const express = require('express');
const router = express.Router();
const TokenService = require('../services/tokenService');
const Twilio = require('../utils/twilio');

module.exports = function (app) {
	app.route('/api/chat').post(async(req, res) => {
		var identity = req.body.identity;
	
		var token = TokenService.generate(identity)
	
		res.json({
		identity: identity,
		token: token.toJwt(),
		});
	});

	app.route('/api/connect/:token').post(async(req, res) => {
		Twilio.Chat.Client.create(req.params.token).then(function(client) {
			tc.messagingClient = client;
			updateConnectedUI();
			tc.loadChannelList(tc.joinGeneralChannel);
			tc.messagingClient.on('channelAdded', $.throttle(tc.loadChannelList));
			tc.messagingClient.on('channelRemoved', $.throttle(tc.loadChannelList));
			tc.messagingClient.on('tokenExpired', refreshToken);
		  });
	});
}
