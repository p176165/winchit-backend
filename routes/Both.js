module.exports = function (app) {
  // Both Routes
  app.use('/api/upload', require('./api/upload'));
  app.use('/api/password', require('./api/password'));
  app.use('/api/auth', require('./api/auth'));
  app.use('/api/users', require('./api/users'));
  app.use('/api/profile', require('./api/profile'));
  app.use('/api/help', require('./api/help'));
  app.use('/api/setLocation', require('./api/setLocation'));
};
