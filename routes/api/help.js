const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');

const Help = require('../../models/Help');
const { check, validationResult } = require('express-validator');

router.post('/sendMessage/:userId', 
  auth,
  [
    check("subject", "Subject is required").not().isEmpty(),
    check("message", "message is required").not().isEmpty(),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ message: 'Some Fields Are Missing' });
    }

    try {
		const user = req.params.userId;
		const subject = req.body.subject;
		const message = req.body.message;

		const help = new Help({
			user,
			subject,
			message
		});

		await help.save();
		res.status(200).json({ message: 'your message has been sent to admin.'});
	} catch(err) {
		console.error(err.message);
      	res.status(500).send("Server error");
	}
  }
)


module.exports = router;
