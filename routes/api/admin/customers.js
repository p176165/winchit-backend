const express = require('express');
const router = express.Router();
const User = require('../../../models/User');
const Configuration = require('../../../models/Configuration');
const isAdmin = require('../../../middleware/isAdmin');
const { check, validationResult } = require("express-validator");
const Vehicle = require("../../../models/Vehicle");
//Twilio
const { twilio } = require("../../../utils/twilio");

router.get('/getCustomers/', isAdmin, async (req, res) => {
	try {
	  const customers = await User.find({ role: 'customer' }).select('-password');
	  res.json(customers);
	} catch (err) {
	  console.error(err.message);
	  res.status(500).send('Server Error');
	} 
});

router.get('/getUsersDashboardData/', isAdmin, async (req, res) => {
	try {
    
	  const totalCustomers = await User.countDocuments(User.find({ role: 'customer' }));
	  const totalActiveCustomers = await User.countDocuments(User.find({ role: 'customer',isVerified: true }));
	  const totalInActiveCustomers = await User.countDocuments(User.find({role: 'customer', isVerified: false }));
	  const bannedCustomers = await User.countDocuments(User.find({ banned: true }));
	  const mostRecentUsers = await User.find({ role: 'customer' }).select('-password').sort({'_id':-1}).limit(5);
	  
	  res.json({
		  customersTotal : totalCustomers,
		  customersActiveTotal:totalActiveCustomers,
		  customersInActiveTotal:totalInActiveCustomers,
		  customersBanned:bannedCustomers,
		  recentUsers:mostRecentUsers
	  });
	} catch (err) {
	  console.error(err.message);
	  res.status(500).send('Server Error');
	} 
});

  
router.get('/getCustomer/:id', isAdmin, async (req, res) => {
	try {
		const customer = await User.find({ _id: req.params.id }).select('-password');
		res.json(customer);
	} catch (err) {
		console.error(err.message);
		res.status(500).send('Server Error');
	}
});

router.get('/getCustomerVehical/:id', isAdmin, async (req, res) => {
	try {
		const customerVehical = await Vehicle.find({ user: req.params.id });
		res.json(customerVehical);
	} catch (err) {
		console.error(err.message);
		res.status(500).send('Server Error');
	}
});

router.post('/updateCustomer/:id', isAdmin, 
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ message: 'Some Fields Are Missing' });
    }

    try {
      updateUser = {};
      updateUser.firstName = req.body.firstName;
      updateUser.surName = req.body.surName;
      updateUser.email = req.body.email;
      updateUser.phone = req.body.phone;
      updateUser['profile.customer.vehicle.registration'] = req.body.vehicleRegistration;
      updateUser['profile.driver.vehicle.make'] = req.body.maker;

      await User.findByIdAndUpdate(req.params.id, updateUser, function(err) {
        if(err) {
          return err.send('Server Error');
        }
      });
      res.status(200).json({message: 'Updated Customer Successfully'});
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
});

router.get('/deleteCustomer/:id', isAdmin, async (req, res) => {
	try {
	  const user = await User.findOneAndDelete({_id: req.params.id});
	  res.status(200).json(user);
	} catch (err) {
	  console.error(err.message);
	  res.status(500).send('Server Error');
	}
});

router.get("/otp/:id",isAdmin, async (req, res) => {
  const customer = await User.findById( req.params.id ).select('-password');
  
  twilio.messages
    .create({
      body: "Your OTP is " + customer.otp.code,
      from: "+447401232103",
      to: customer.phone,
    })
    .then((message) => {
      console.log(message.sid);
      res.status(200).json({ message: "OTP has been sent to your phone number" });
    }).catch(err => (console.log(err)));
});

router.get('/getLowRatedCustomers', isAdmin, async (req, res) => {
  try {
    const configuration = await Configuration.findOne();
	  const lowRatingCustomers = await User.find({ role: 'customer', 'rating.currentRating': {$lte:configuration.customerLowRatingLimit}}).sort({'rating.currentRating': -1}).select('-password');
    res.json(lowRatingCustomers);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

router.get('/getHighRatedCustomers', isAdmin, async (req, res) => {
  try {
    const configuration = await Configuration.findOne();
	  const highRatingCustomers = await User.find({ role: 'customer', 'rating.currentRating': {$gte:configuration.customerHighRatingLimit}}).sort({'rating.currentRating': -1}).select('-password');
    res.json(highRatingCustomers);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

module.exports = router;
