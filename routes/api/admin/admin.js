const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const Admin = require('../../../models/Admin');

router.post(
	"/addAdmin",
	async (req, res) => {
		const name = req.body.name;
		const email = req.body.email;
		const password = req.body.password;

		try {
			let admin = new Admin({
				name,
				email,
				password
			});

			const salt = await bcrypt.genSalt(10);
			admin.password = await bcrypt.hash(password, salt);
			await admin.save();
		} catch (err) {
			console.error(err.message);
			res.status(500).send("Server error");
		}
	}
);