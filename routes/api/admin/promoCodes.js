const express = require('express');
const router = express.Router();
const PromoCode = require('../../../models/PromoCode');
const isAdmin = require('../../../middleware/isAdmin');
const { check, validationResult } = require("express-validator");

router.post("/addPromoCode", isAdmin,
	async (req, res) => {
		var data = new Date(); 
		console.log(data);
		const title = req.body.title;
		const code = req.body.code;
		const discount = req.body.discount;
		const percentage = req.body.percentage;
		const start_date = req.body.start_date;
		const expire_on = req.body.expire_on;

		try {
			let promoCode = new PromoCode({
				title,
				code,
				discount,
				percentage,
				start_date,
				expire_on
			});

			await promoCode.save();
			res.status(200).json({data: promoCode})
		} catch (err) {
			console.error(err.message);
			res.status(500).send("Server error");
		}
	}
);

router.get('/getPromoCodes/', isAdmin, async (req, res) => {
	try {
	  const promoCodes = await PromoCode.find();
	  res.status(200).json(promoCodes);
	} catch (err) {
	  console.error(err.message);
	  res.status(500).send('Server Error');
	}
});

router.get('/getPromoCode/:id', isAdmin, async (req, res) => {
	try {
	  const promoCode = await PromoCode.find({_id: req.params.id});
	  res.status(200).json(promoCode);
	} catch (err) {
	  console.error(err.message);
	  res.status(500).send('Server Error');
	}
});

router.post('/updatePromoCode/:id', isAdmin, 
  [
    check("title", "Title is required").not().isEmpty(),
	check("code", "Promocode is required").not().isEmpty(),
	check("discount", "Discount Value is required").not().isEmpty(),
	check("start_date", "Start Date for promo code is required").not().isEmpty(),
	check("expire_on", "Expire Date for promo code is required").not().isEmpty(),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ message: 'Some Fields Are Missing' });
    }

    try {
      updatePromo = {};
      updatePromo.title = req.body.title;
      updatePromo.code = req.body.code;
      updatePromo.discount = req.body.discount;
	  updatePromo.percentage = req.body.percentage;
	  updatePromo.start_date = req.body.start_date;
	  updatePromo.expire_on = req.body.expire_on;

      await PromoCode.findByIdAndUpdate(req.params.id, updatePromo, function(err) {
        if(err) {
          return err.send('Server Error');
        }
      });
      res.status(200).json({message: 'Updated Promo Code Successfully'});
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
});

router.get('/deletePromoCode/:id', isAdmin, async (req, res) => {
	try {
	  const promoCode = await PromoCode.findOneAndDelete({_id: req.params.id});
	  res.status(200).json(promoCode);
	} catch (err) {
	  console.error(err.message);
	  res.status(500).send('Server Error');
	}
});

module.exports = router;