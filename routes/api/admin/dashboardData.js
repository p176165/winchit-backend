const express = require('express');
const router = express.Router();
const User = require('../../../models/User');
const Request = require('../../../models/Request');
const PromoCode = require('../../../models/PromoCode');
const Configuration = require('../../../models/Configuration');
const isAdmin = require('../../../middleware/isAdmin');
const { check, validationResult } = require("express-validator");

router.get('/getDashboardData/', isAdmin, async (req, res) => {
	try {
	  const customers = await User.find({ role: 'customer' });
	  const promoCodes = await PromoCode.find();
	  const pendingDriverRequests = await User.find({ role: 'driver', isVerified: false });
	  const approvedDriverRequests = await User.find({ role: 'driver', isVerified: true });
	  const availableRequests = await Request.find({ status: 'Available' });
	  const inProgressRequests = await Request.find({ status: 'InProgress' });
	  const completedRequests = await Request.find({ status: 'Completed' });
	  const assignedRequests = await Request.find({ status: 'Assigned' });
	  const configuration = await Configuration.findOne();
	  const lowRatingDrivers = await User.find({ role: 'driver', 'rating.currentRating': {$lte:configuration.driverLowRatingLimit}}).sort({'rating.currentRating': -1}).limit(5).select('-password');
	  const lowRatingCustomers = await User.find({ role: 'customer', 'rating.currentRating': {$lte:configuration.customerLowRatingLimit}}).sort({'rating.currentRating': -1}).limit(5).select('-password');
	  const highRatingDrivers = await User.find({ role: 'driver', 'rating.currentRating': {$gte:configuration.driverHighRatingLimit}}).sort({'rating.currentRating': -1}).limit(5).select('-password');
          const highRatingCustomers = await User.find({ role: 'customer', 'rating.currentRating': {$gte:configuration.customerHighRatingLimit}}).sort({'rating.currentRating': -1}).limit(5).select('-password');
	  res.status(200).json({customers: customers.length, pendingDriverRequests: pendingDriverRequests.length, approvedDriverRequests: approvedDriverRequests.length, promoCodes: promoCodes.length, availableRequests: availableRequests.length, inProgressRequests: inProgressRequests.length, completedRequests: completedRequests.length, assignedRequests: assignedRequests.length, lowRatingCustomers: lowRatingCustomers, lowRatingDrivers: lowRatingDrivers, highRatingDrivers: highRatingDrivers, highRatingCustomers: highRatingCustomers});
	} catch (err) {
	  console.error(err.message);
	  res.status(500).send('Server Error');
	}
});

module.exports = router;

