const express = require('express');
const router = express.Router();
const User = require('../../../models/User');
const PinRequest = require('../../../models/PinRequest');
const Request = require('../../../models/Request');
const isAdmin = require('../../../middleware/isAdmin');
const { check, validationResult } = require("express-validator");

router.get('/pending', isAdmin, async (req, res) => {
	try {
		const pendingPinRequests = await PinRequest.find({ solved: false }).populate({ path: 'requestId', populate: [{ path: 'customer' }, { path: 'driverId' }] });
	  	res.json({msg: 'pending pin requests', data: pendingPinRequests});
	} catch (err) {
	  	console.error(err.message);
	  	res.status(500).send('Server Error');
	}
});

router.get('/completed', isAdmin, async (req, res) => {
        try {
                const completedPinRequests = await PinRequest.find({ solved: true }).populate({ path: 'requestId', populate: [{ path: 'customer' }, { path: 'driverId' }] });
                res.json({msg: 'completed pin requests', data: completedPinRequests});
        } catch (err) {
                console.error(err.message);
                res.status(500).send('Server Error');
        }
});

router.get('/markRequestAsCompleted/:requestId', isAdmin, async (req, res) => {
	try {
		await PinRequest.findByIdAndUpdate(req.params.requestId, {solved: true}, function(err, request) {
			if(err) {
				return res.json({msg: err});
			}
			Request.findByIdAndUpdate(request.requestId, {status: "Completed"}, function(err) {
			if(err) {
                                return res.json({msg: err});
                        }
			})
		});
	  	res.json({msg: 'request marked as completed'});
	} catch (err) {
	  	console.error(err.message);
	  	res.status(500).send('Server Error');
	}
});

module.exports = router;
