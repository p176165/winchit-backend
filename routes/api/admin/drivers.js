const express = require('express');
const router = express.Router();
const User = require('../../../models/User');
const Configuration = require('../../../models/Configuration');
const isAdmin = require('../../../middleware/isAdmin');
const { check, validationResult } = require("express-validator");

//Twilio
const { twilio } = require("../../../utils/twilio");

// @route GET api/admin/users/drivers
// @desc Get All Drivers
// @access Admin
router.get('/getDrivers/', isAdmin, async (req, res) => {
	try {
	  const customers = await User.find({ role: 'driver' }).select('-password');
	  res.json(customers);
	} catch (err) {
	  console.error(err.message);
	  res.status(500).send('Server Error');
	} 
});

router.get('/pendingRequests/', isAdmin, async (req, res) => {
  try {
    const pendingRequests = await User.find({ role: 'driver', isVerified: false }).select('-password');
    res.json(pendingRequests);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

router.get('/approvedRequests/', isAdmin, async (req, res) => {
  try {
    const approvedRequests = await User.find({ role: 'driver', isVerified: true }).select('-password');
    res.json(approvedRequests);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

router.get('/getDriver/:id', isAdmin, async (req, res) => {
  try {
    const driver = await User.find({ _id: req.params.id }).select('-password').populate('vehicles');
    res.json(driver);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

router.get('/approveDriver/:id', isAdmin, async (req, res) => {
  try {
    await User.findByIdAndUpdate(req.params.id, {isVerified: true}, function(err) {
      if(err) {
        return err;
      }
    });
    res.status(200).json({message: 'Request is approved Successfully'});
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

router.get('/changeBusyStatus/:id', isAdmin, async (req, res) => {
  try {
    const user = await User.findById(req.params.id).select('-password');
    if(user.is_busy) {
      user.is_busy = false;
    } else {
      user.is_busy = true;
    }

    await user.save();

    res.status(200).json({message: 'Status changed successfully'});
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

router.get('/disapproveDriver/:id', isAdmin, async (req, res) => {
  try {
    await User.findByIdAndUpdate(req.params.id, {isVerified: false}, function(err) {
      if(err) {
        return err;
      }
    });
    res.status(200).json({message: 'Request is approved Successfully'});
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

router.post('/updateDriver/:id', isAdmin, 
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ message: 'Some Fields Are Missing' });
    }

    try {
      updateUser = {};
      updateUser.firstName = req.body.firstName;
      updateUser.surName = req.body.surName;
      updateUser.email = req.body.email;
      updateUser.phone = req.body.phone;
      updateUser.yearsOfExperience = req.body.yearsOfExperience;
      updateUser['profile.driver.business.businessName']= req.body.businessName;
      updateUser['profile.driver.business.businessAddress'] = req.body.businessAddress;
      updateUser['profile.driver.business.city'] = req.body.businessCity;
      updateUser['profile.driver.business.postCode'] = req.body.businessPostalCode;
      updateUser['profile.driver.vehicle.Type'] = req.body.vehicleType;
      updateUser['profile.driver.vehicle.registration'] = req.body.vehicleRegistration;
      updateUser['profile.driver.vehicle.make'] = req.body.maker;
      updateUser['profile.driver.vehicle.maxPassengers'] = req.body.maxPassengers;
      updateUser['rating.currentRating'] = req.body.currentRating;
      updateUser['rating.numberOfRating'] = req.body.numberOfRatings;
      updateUser['wallet.balance'] = req.body.walletBalance;
      updateUser['profile.driver.license.licenseNumber'] = req.body.licenseNumber;
      updateUser['profile.driver.license.expiryDate'] = req.body.licenseExpiry;
      updateUser['profile.driver.insurance.expiryDate'] = req.body.insuranceExpiry;
      updateUser['otp.code'] = req.body.otp;

      await User.findByIdAndUpdate(req.params.id, updateUser, function(err) {
        if(err) {
          return err.send('Server Error');
        }
      });
      res.status(200).json({message: 'Updated Driver Successfully'});
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
});

router.get('/deleteDriver/:id', isAdmin, async (req, res) => {
	try {
	  const user = await User.findOneAndDelete({_id: req.params.id});
	  res.status(200).json(user);
	} catch (err) {
	  console.error(err.message);
	  res.status(500).send('Server Error');
	}
});

router.get("/otp/:id", isAdmin, async (req, res) => {
  const driver = await User.findById( req.params.id ).select('-password');
  twilio.messages
    .create({
      body: "Your OTP is " + driver.otp.code,
      from: "+447401232103",
      to: driver.phone,
    })
    .then((message) => {
      console.log(message.sid);
      res.status(200).json({message: "OTP has been sent to your phone number"});
    }).catch(err => (console.log(err)));
});

router.get('/getLowRatedDrivers', isAdmin, async (req, res) => {
  try {
    const configuration = await Configuration.findOne();
	  const lowRatingDrivers = await User.find({ role: 'driver', 'rating.currentRating': {$lte:configuration.driverLowRatingLimit}}).sort({'rating.currentRating': -1}).select('-password');
    res.json(lowRatingDrivers);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

router.get('/getHighRatedDrivers', isAdmin, async (req, res) => {
  try {
    const configuration = await Configuration.findOne();
	  const highRatingDrivers = await User.find({ role: 'driver', 'rating.currentRating': {$gte:configuration.driverHighRatingLimit}}).sort({'rating.currentRating': -1}).select('-password');
    res.json(highRatingDrivers);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

module.exports = router;
