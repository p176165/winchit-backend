const express = require('express');
const router = express.Router();
const Configuration = require('../../../models/Configuration');
const isAdmin = require('../../../middleware/isAdmin');
const { check, validationResult } = require("express-validator");

router.post("/configuration", isAdmin,
	[
		check("companyCommission", "Company Commission is required").not().isEmpty(),
		check("driverLowRatingLimit", "Driver Low Rating Limit is required").not().isEmpty(),
		check("customerLowRatingLimit", "Customer Low Rating Limit is required").not().isEmpty(),
		check("driverHighRatingLimit", "Driver High Rating Limit is required").not().isEmpty(),
		check("customerHighRatingLimit", "Customer High Rating Limit is required").not().isEmpty(),
		check("searchRangeForDriver", "Search Range For Driver is required").not().isEmpty(),
	], 
	async (req, res) => {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ message: 'Some Fields Are Missing' });
		}

		const companyCommission = req.body.companyCommission;
		const driverLowRatingLimit = req.body.driverLowRatingLimit;
		const customerLowRatingLimit = req.body.customerLowRatingLimit;
		const driverHighRatingLimit = req.body.driverHighRatingLimit;
		const customerHighRatingLimit = req.body.customerHighRatingLimit;
		const searchRangeForDriver = req.body.searchRangeForDriver;

		try {
			const configuration = await Configuration.findOne();

			if(!configuration) {
				let configure = new Configuration({
					companyCommission,
					driverLowRatingLimit,
					customerLowRatingLimit,
					driverHighRatingLimit,
					customerHighRatingLimit,
					searchRangeForDriver
				});

				await configure.save();
			} else {
				updateConfiguration = {};
				updateConfiguration.companyCommission = companyCommission;
				updateConfiguration.driverLowRatingLimit = driverLowRatingLimit;
				updateConfiguration.customerLowRatingLimit = customerLowRatingLimit;
				updateConfiguration.driverHighRatingLimit = driverHighRatingLimit;
				updateConfiguration.customerHighRatingLimit = customerHighRatingLimit;
				updateConfiguration.searchRangeForDriver = searchRangeForDriver;

				await Configuration.findByIdAndUpdate(configuration._id, updateConfiguration, function(err) {
					if(err) {
						return err.send('Server Error');
					}
				});
			}
			res.status(200).json({msg: 'Configuration updated successfully'})
		} catch (err) {
			console.error(err.message);
			res.status(500).send("Server error");
		}
	}
);

router.get('/getConfiguration', isAdmin, async (req, res) => {
	try {
	  const configuration = await Configuration.findOne();
	  if(!configuration) {
		res.status(404).json({msg: 'no configuration setting found'});
	  } else {
		res.status(200).json({msg: 'configuration data', data: configuration});
	  }
	} catch (err) {
	  console.error(err.message);
	  res.status(500).send('Server Error');
	}
});

module.exports = router;
