const express = require('express');
const router = express.Router();
const Request = require('../../../models/Request');
const isAdmin = require('../../../middleware/isAdmin');

router.get('/completed/', isAdmin, async (req, res) => {
	try {
	  const completedRides = await Request.find({ status: 'Completed' }).populate('driverId').populate('customer').populate('quotes.driverId').sort({createdAt: -1});
	  res.status(200).json(completedRides);
	} catch (err) {
	  console.error(err.message);
	  res.status(500).send('Server Error');
	}
});

router.get('/available/', isAdmin, async (req, res) => {
	try {
	  const availableRides = await Request.find({ status: 'Available' }).populate('driverId').populate('customer').populate('quotes.driverId').sort({createdAt: -1});
	  res.status(200).json(availableRides);
	} catch (err) {
	  console.error(err.message);
	  res.status(500).send('Server Error');
	}
});

router.get('/inProgress/', isAdmin, async (req, res) => {
	try {
	  const inProgressRides = await Request.find({ status: 'InProgress' }).populate('driverId').populate('customer').sort({createdAt: -1});
	  res.status(200).json(inProgressRides);
	} catch (err) {
	  console.error(err.message);
	  res.status(500).send('Server Error');
	}
});

router.get('/cancelled/', isAdmin, async (req, res) => {
	try {
	  const cancelledRides = await Request.find({ $or: [{status: 'CancelledByCustomer' }, {status: 'CancelledByDriver'}] }).populate('driverId').populate('customer').sort({createdAt: -1});
	  res.status(200).json(cancelledRides);
	} catch (err) {
	  console.error(err.message);
	  res.status(500).send('Server Error');
	}
});


module.exports = router;
