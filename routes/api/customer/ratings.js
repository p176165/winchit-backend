const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');

const User = require('../../../models/User');

const auth = require('../../../middleware/auth');
const isOwnerAndCustomer = require('../../../middleware/isOwnerAndCustomer');
const isRequestCompleted = require('../../../middleware/isRequestCompleted');

// @route POST api/ratings/customer/:requestId
// @desc Customer set Rating of Driver
// @access Customer
router.post(
  '/:requestId',
  [
    auth,
    isOwnerAndCustomer,
    isRequestCompleted,
    [check('rating', 'Enter Valid Rating').isInt({ min: 1, max: 5 })],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      let request = req.request;

      const { rating, comment, tip } = req.body;

      let driver = await User.findById(request.driverId);

      if (request.feedbackByCustomer.rating) {
        // Update old feedback

        driver.rating.currentRating =
          (driver.rating.currentRating * driver.rating.numberOfRating -
            request.feedbackByCustomer.rating +
            rating) /
          driver.rating.numberOfRating;
      } else {
        // New feedback
        driver.rating.currentRating =
          (driver.rating.currentRating * driver.rating.numberOfRating +
            rating) /
          (driver.rating.numberOfRating + 1);

        driver.rating.numberOfRating = driver.rating.numberOfRating + 1;
      }

      request.customerRateDriver = req.params.rate;

      request.feedbackByCustomer.rating = rating;
      // Comment
      if (comment != null) {
        request.feedbackByCustomer.comment = comment;
      }
      // Tip
      if (tip != null) {
        request.feedbackByCustomer.tip = tip;
      }

      await driver.save();
      await request.save();

      res.json({ msg: 'Driver Rated' });
    } catch (err) {
      console.error(err.message);
      res.json({ msg: 'Server Error' });
    }
  }
);

module.exports = router;
