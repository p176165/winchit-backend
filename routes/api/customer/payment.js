const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');

const User = require('../../../models/User');
const isOwnerAndCustomer = require('./../../../middleware/isOwnerAndCustomer');
const Transaction = require('../../../models/Transaction');

const auth = require('../../../middleware/auth');
const hasPaymentSetup = require('../../../middleware/hasPaymentSetup');
const stripe = require('stripe')(
  'sk_test_51HQtLcLx4TQNDhPV83EKbjYg7lFRaLQNtFspqfa4fzIFdabY4EllAGzRBS5EwVo0kWsvFYEnU5hAom6rBXlr3UHb000njUmIrJ'
);

// @route GET api/customer/payment/cards
// @desc Get All Cards of Customer
// @access Customer
router.get('/cards/', [auth, hasPaymentSetup], async (req, res) => {
  try {
    const paymentMethods = await stripe.customers.listSources(req.stripeId, {
      object: 'card',
      limit: 10,
    });
    res.json(paymentMethods);
  } catch (error) {
    return res.json({ msg: error.message });
  }
});

// @route POST api/customer/payment/pay/:requestId
// @desc Pay for Ride
// @access Customer
router.post(
  '/pay/:requestId',
  [
    auth,
    hasPaymentSetup,
    isOwnerAndCustomer,
    [check('token', 'Token is required').not().isEmpty()],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      // Get Price from Quote
      const quotesOnRequest = req.request.quotes;
      console.log(quotesOnRequest);
      console.log(req.request.selectedQuoteId);
      const selectedQuote = await quotesOnRequest.find((el) =>
        el._id.equals(req.request.selectedQuoteId)
      );

      const price = (req.request.tip || 0) + selectedQuote.price;
      console.log('price');

      const charge = await stripe.charges.create({
        amount: price * 100, // Convert Dollars to Cents
        currency: 'usd',
        customer: req.stripeId,
        source: req.body.token,

        description: 'My First Test Charge (created for API docs)',
      });
      let transaction = new Transaction({
        request: req.request._id,
        tip: req.request.tip,
        customerPaid: charge.amount,
        chargeId: charge.id,
      });
      transaction.save();
      return res.json({ msg: 'Transaction Completed' });
    } catch (err) {
      return res.json({ msg: err.message });
    }
  }
);

// @route POST api/customer/payment/cards/
// @desc Add Card to ID
// @access Customer
router.post(
  '/cards/',
  [
    auth,
    hasPaymentSetup,
    [check('token', 'Token is required').not().isEmpty()],
  ],
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }
      // const intent = await stripe.setupIntents.create({
      //   customer: req.stripeId,
      //   usage: "on_session",
      // });
      const card = await stripe.customers.createSource(req.stripeId, {
        source: req.body.token,
      });
      res.json({
        data: card,
        description:
          'Use the client_secret to add the card, you need to use the Stripe frontend API to do it',
      });
    } catch (error) {
      return res.json({ msg: error.message });
    }
  }
);

module.exports = router;
