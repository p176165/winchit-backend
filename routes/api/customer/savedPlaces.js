const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');
const auth = require('../../../middleware/auth');
const SavedPlaces = require('../../../models/SavedPlaces');

router.post(
  '/add',
  auth,
  [
    check('title', 'title is required').not().isEmpty(),
    check('address', 'address is required').not().isEmpty()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const addressCoordinates = [
        req.body.address.coordinates.split(',')[1],
        req.body.address.coordinates.split(',')[0]
      ];

      const location = {
        type: 'Point',
        coordinates: addressCoordinates
      };

      const address = req.body.currentAddress;
      const city = req.body.currentCity;
      const title = req.body.title;

      let savePlace = new SavedPlaces({
        userId: req.user._id,
        title,
        location,
        address,
        city
      });
  
      await savePlace.save(function(err, saved) {
        if(err) return res.status(500).json({msg: 'error occured while saving place'});

        res.status(201).json({ msg: 'place is saved successfully', data: savePlace });
      })
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

router.get('/', auth, async (req, res) => {
  try {
    const savedPlaces = await SavedPlaces.find({userId: req.user._id});

    res.status(200).json({ msg: "list of saved places", data: savedPlaces });
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

router.get('/getPlace/:id', auth, async (req, res) => {
  try {
    const place = await SavedPlaces.findById(req.params.id);

    res.status(200).json({ savedPlace: place });
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

router.put('/updatePlace/:id', auth, 
  [
    check('title', 'title is required').not().isEmpty(),
    check('address', 'address is required').not().isEmpty()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ message: 'Some Fields Are Missing' });
    }

    try {
      const addressCoordinates = [
        req.body.address.coordinates.split(',')[1],
        req.body.address.coordinates.split(',')[0]
      ];

      const location = {
        type: 'Point',
        coordinates: addressCoordinates
      };

      const address = req.body.currentAddress;
      const city = req.body.currentCity;
      const title = req.body.title;

      let updatePlace = {
        title: title,
        city: city,
        address: address,
        location: location
      };
  
      await SavedPlaces.findByIdAndUpdate(req.params.id, updatePlace, {new: true}, function(err, place) {
        if(err) return res.status(500).json({msg: 'error occured while updating place'});
        res.status(200).json({ msg: 'place is updated successfully', data: place });
      })
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
});

router.delete('/:id', auth, async (req, res) => {
  try {
    await SavedPlaces.findByIdAndDelete(req.params.id);

    res.status(200).json({ msg: "place deleted" });
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

module.exports = router;