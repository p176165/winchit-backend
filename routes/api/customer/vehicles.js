const express = require('express');
const router = express.Router();
const auth = require('../../../middleware/auth');
const { check, validationResult } = require('express-validator');
const Vehicle = require('../../../models/Vehicle');

// @route GET api/driver/vehicles
// @desc TEST route
// @access Public
router.get('/userVehicles', auth, async (req, res) => {
  try {
    let vehicles = await Vehicle.find({user: req.user._id});

    res.status(200).json({ vehicle: vehicles });
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

// @route POST api/driver/vehicles
// @desc Update vehicle details
// @access Driver
router.post(
  '/addVehicle',
  auth,
  [
    check('vehicleMake', 'vehicleMake is required').not().isEmpty(),
    check('vehicleRegistration', 'vehicleRegistration is required').not().isEmpty()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const vehicleData = {
        user: req.user._id,
        registration: req.body.vehicleRegistration,
        make: req.body.vehicleMake
      };

      const vehicle = new Vehicle(vehicleData)

      await vehicle.save();
      res.status(200).json({ msg: 'vehicle added' });
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

router.get('/getVehicle/:id', auth, async (req, res) => {
  try {
    const vehicle = await Vehicle.findById(req.params.id);

    res.status(200).json({ vehicle: vehicle });
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

router.put('/updateVehicle/:id', auth, 
  [
    check('vehicleMake', 'vehicleMake is required').not().isEmpty(),
    check('vehicleRegistration', 'vehicleRegistration is required').not().isEmpty(),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ message: 'Some Fields Are Missing' });
    }

    try {
      const vehicleData = {
        registration: req.body.vehicleRegistration,
        make: req.body.vehicleMake,
      };

      await Vehicle.findByIdAndUpdate(req.params.id, vehicleData, function(err) {
        if(err) {
          return err.send('Server Error');
        }
      });
      res.status(200).json({message: 'Vehicle updated Successfully'});
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
});

router.delete('/deleteVehicle/:id', auth, async (req, res) => {
  try {
    await Vehicle.findByIdAndDelete(req.params.id);

    res.status(200).json({ msg: 'Vehicle deleted successfully' });
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

module.exports = router;

