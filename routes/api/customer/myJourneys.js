const express = require('express');
const router = express.Router();
const auth = require('../../../middleware/auth');

const Request = require('../../../models/Request');

router.get('/all', auth,
  async (req, res) => {
    try {
		const completed = await Request.find({ customer: req.user._id, status: 'Completed' }).sort({createdAt: -1}).populate('driverId').populate('customer').populate('quotes.driverId');
		const available = await Request.find({ customer: req.user._id, status: 'Available', requestType: 'scheduled' }).sort({createdAt: -1}).populate('customer').populate('driverId').populate('quotes.driverId');
		const cancelled = await Request.find({ customer: req.user._id, status: 'Cancelled' }).populate('driverId').sort({createdAt: -1}).populate('customer');
		const scheduled = await Request.find({ customer: req.user._id, status: 'Assigned', requestType: 'scheduled', driverId: { $ne: null} }).sort({createdAt: -1}).populate('customer').populate('driverId');
		res.status(200).json({ message: 'my journeys data', completed: completed, available: available, cancelled: cancelled, scheduled: scheduled});
	} catch(err) {
		console.error(err.message);
      	res.status(500).send("Server error");
	}
  }
)


module.exports = router;
