const express = require("express");
const router = express.Router();
const auth = require("../../../middleware/auth");
const User = require("../../../models/User");
const bcrypt = require("bcryptjs");
const randomString = require("randomstring");
const { check, validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
const config = require("config");
const passport = require("passport");
const keys = require("../../../config/keys");

//Twilio
const { twilio } = require("../../../utils/twilio");

// @route GET api/customer/auth
// @desc Get User Details
// @access Cusotmer
router.get("/", auth, async (req, res) => {
  try {
    const user = await User.findById(req.user.id).select("-password");
    res.json(user);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

// @route GET api/customer/auth/login
// @desc Authenticate Login Local
// @access Public
router.post(
  "/login/local",
  [
    check("phone", "Please include a valid phone number").not().isEmpty(),
    check("password", "Please enter password").exists(),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { phone, password } = req.body;

    try {
      // Check to see if user exists
      let user = await User.findOne({
        phone,
        role: "customer",
        method: "local",
      });

      if (!user) {
        return res.status(400).json({
          errors: [{ msg: "No User Exists with this Phone Number" }],
        });
      }

      const isMatch = await bcrypt.compare(password, user.password);

      if (!isMatch) {
        return res
          .status(400)
          .json({ errors: [{ msg: "Incorrect password" }] });
      }

      if (user.banned) {
        return res
          .status(400)
          .json({ errors: [{ msg: "You are banned by the admin. Please contact admin for further details" }] });
      }

      const payload = {
        user: {
          id: user.id,
        },
      };
      // Return JsonwebToken
      jwt.sign(
        payload,
        config.get("jwtToken"),
        { expiresIn: config.get("jwtExpireTime") },
        async (err, token) => {
          if (err) throw err;
          if (req.body.registerationToken) {
            user.registerationTokens = [req.body.registerationToken];
            await user.save();
          }
          res.json({ token, verified: user.isVerified });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

// @route GET api/customer/auth/login
// @desc Authenticate Login
// @access Public
router.post(
  "/login/google",
  passport.authenticate("googleTokenLogin", { session: false }),
  async (req, res) => {
    try {
      const payload = {
        user: {
          id: req.user._id,
        },
      };
      // Return JsonwebToken
      jwt.sign(
        payload,
        config.get("jwtToken"),
        { expiresIn: config.get("jwtExpireTime") },
        async (err, token) => {
          if (err) throw err;
          if (req.body.registerationToken) {
            req.user.registerationTokens = [req.body.registerationToken];
            await req.user.save();
          }
          res.json({ token });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

// @route GET api/customer/auth/login/facebook
// @desc Authenticate Login facebook
// @access Public
router.post(
  "/login/facebook",
  passport.authenticate("facebookTokenLogin", { session: false }),
  async (req, res) => {
    try {
      const payload = {
        user: {
          id: req.user._id,
        },
      };
      // Return JsonwebToken
      jwt.sign(
        payload,
        config.get("jwtToken"),
        { expiresIn: config.get("jwtExpireTime") },
        async (err, token) => {
          if (err) throw err;
          if (req.body.registerationToken) {
            req.user.registerationTokens = [req.body.registerationToken];
            await req.user.save();
          }
          res.json({ token });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

// @route GET api/customer/auth/login/twitter
// @desc Authenticate Login Twitter
// @access Public
router.post(
  "/login/twitter",
  passport.authenticate("twitterTokenLogin", { session: false }),
  async (req, res) => {
    try {
      const payload = {
        user: {
          id: req.user._id,
        },
      };
      // Return JsonwebToken
      jwt.sign(
        payload,
        config.get("jwtToken"),
        { expiresIn: config.get("jwtExpireTime") },
        async (err, token) => {
          if (err) throw err;
          if (req.body.registerationToken) {
            req.user.registerationTokens = [req.body.registerationToken];
            await req.user.save();
          }
          res.json({ token });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

// @route POST api/customer/auth/otp
// @desc Send OTP to users cellphone
// @access Customer
router.post("/otp", auth, async (req, res) => {
  // return res.send({name:req.params.num})
  twilio.messages
    .create({
      body: "Your OTP is " + req.user.otp.code,
      from: "+447401232103",
      to: req.user.phone,
    })
    .then((message) => {
      console.log(message.sid);
      res.json("OTP has been sent to your phone number");
    });
});

// @route GET api/customer/auth/otp/verify/:otp
// @desc Verify OTP
// @access Customer
router.get("/otp/verify/:otp", auth, function (req, res) {
  User.findOne(
    { $and: [{ _id: req.user.id }, { "otp.code": req.params.otp }] },
    function (err, foundUser) {
      if (err) {
        res.status(500).send("Invalid Link");
      } else {
        if (!foundUser || foundUser.length <= 0) {
          res.status(500).send("Wrong OTP");
        } else {
          foundUser.isVerified = true;
          foundUser.otp = {
            code: randomString.generate({ length: 4, charset: "numeric" }),
            lastSent: Date.now(),
          };

          foundUser.save(function (err) {
            if (err) {
              res.status(500).send("Some error occured");
            } else {
              res.json({ message: "account has been verified" });
            }
          });
        }
      }
    }
  );
});

module.exports = router;
