const express = require('express');
const router = express.Router();
const randomString = require('randomstring');

const Request = require('../../../../../models/Request');
const auth = require('../../../../../middleware/auth');
const User = require('../../../../../models/User');
const geolib = require('geolib');

const { check, validationResult } = require('express-validator');

const isOwnerAndCustomer = require('../../../../../middleware/isOwnerAndCustomer');
const isCustomer = require('../../../../../middleware/isCustomer');
const isRequestAvailable = require('../../../../../middleware/isRequestAvailable');
const isRequestAvailableOrAssigned = require('../../../../../middleware/isRequestAvailableOrAssigned');

// @route POST api/customer/relocate/rides/new
// @desc Create a relocate Request
// @access Customer
router.post(
  '/new',
  [
    auth,
    isCustomer,
    [
      check('dropoffDate', 'Dropoff date is required').not().isEmpty(),
      check('vehicleMake', 'Vehicle make is required').not().isEmpty(),
      check('vehicleRegistration', 'vehicle Registration is required').not().isEmpty(),
      check(
        'numberOfPassengers',
        'Number of passengers is required'
      ).isNumeric(),
      check(
        'pickupLocation.coordinates',
        'Pickup location is wrong'
      ).isLatLong(),
      check(
        'pickupKeys.coordinates',
        'Pickup key location is wrong'
      ).isLatLong(),
      check(
        'dropoffLocation.coordinates',
        'Dropoff location is wrong'
      ).isLatLong(),
    ],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const pickupCoordinates = [
        req.body.pickupLocation.coordinates.split(',')[1],
        req.body.pickupLocation.coordinates.split(',')[0],
      ];
      const pickupKeysCoordinates = [
        req.body.pickupKeys.coordinates.split(',')[1],
        req.body.pickupKeys.coordinates.split(',')[0],
      ];
      const dropoffCoordinate = [
        req.body.dropoffLocation.coordinates.split(',')[1],
        req.body.dropoffLocation.coordinates.split(',')[0],
      ];

      const pickupLocation = {
        type: 'Point',
        coordinates: pickupCoordinates,
      };
      const pickupKeysLocation = {
        type: 'Point',
        coordinates: pickupKeysCoordinates,
      };
      const dropoffLocation = {
        type: 'Point',
        coordinates: dropoffCoordinate,
      };
      let keysToPickupDistance = geolib.getPreciseDistance(
        { latitude: pickupKeysCoordinates[0], longitude: pickupKeysCoordinates[1] },
        { latitude: pickupCoordinates[0], longitude: pickupCoordinates[1] }
      );
      const distanceKeysToPickup = parseFloat(keysToPickupDistance)*parseFloat(0.000621);
      let pickupToDropoffDistance = geolib.getPreciseDistance(
        { latitude: pickupCoordinates[0], longitude: pickupCoordinates[1] },
        { latitude: dropoffCoordinate[0], longitude: dropoffCoordinate[1] }
      );
      const distancePickupToDropoff = parseFloat(pickupToDropoffDistance)*parseFloat(0.000621);
      const pin = randomString.generate({ length: 4, charset: 'numeric' });
      const customer = req.user._id;
      const numberOfPassengers = req.body.numberOfPassengers;
      const breakdownType = req.body.breakdownType;
      const requestType = 'scheduled';
      const dropoffDate = req.body.dropoffDate;
      const vehicleRegistration = req.body.vehicleRegistration;
      const vehicleMake = req.body.vehicleMake;
      const isKeyAtPickupLocation = req.body.isKeyAtPickupLocation;

      const pickup = {
        location: pickupLocation,
        address: req.body.pickupLocation.address,
        city: req.body.pickupLocation.city
      };
      const dropoff = {
        location: dropoffLocation,
        address: req.body.dropoffLocation.address,
        city: req.body.dropoffLocation.city
      };

      const pickupKeys = {
        location: pickupKeysLocation,
        address: req.body.pickupKeys.address,
        city: req.body.pickupKeys.city
      };

      let request = new Request({
        breakdownType,
        requestType,
        numberOfPassengers,
        dropoffDate,
        customer,
        dropoff,
        pickup,
        pickupKeys,
        pin,
        distanceKeysToPickup,
        distancePickupToDropoff,
        vehicleMake,
        vehicleRegistration,
        isKeyAtPickupLocation
      });

      await request.save();
      res.json('Request has been saved');
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

// @route POST api/customer/relocate/rides/cancel/:requestId
// @desc Cancel Request
// @access Customer
router.post(
  '/cancel/:requestId',
  [auth, isOwnerAndCustomer, isRequestAvailableOrAssigned],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      let request = req.request;

      request.status = 'Cancelled';
      //send notification to the driver if his qoute was accepted

      await request.save();
      return res.json({ msg: 'Ride has been cancelled' });
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

module.exports = router;
