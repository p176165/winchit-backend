const express = require('express');
const router = express.Router();

const auth = require('../../../../../middleware/auth');

const { check, validationResult } = require('express-validator');

const isOwnerAndCustomer = require('../../../../../middleware/isOwnerAndCustomer');
const isRequestAvailable = require('../../../../../middleware/isRequestAvailable');

// @route POST api/customer/relocate/quotes/select/:requestId/:quoteId
// @desc Select a Quote
// @access Customer
router.post(
  '/select/:requestId/:quoteId',
  [auth, isOwnerAndCustomer, isRequestAvailable],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const request = req.request;

      const { quoteId } = req.params;

      let foundQuote = await request.quotes.filter((el) => el._id == quoteId);

      if (foundQuote.length < 1) {
        return res.json({ msg: 'Quote Not Found' });
      }
      request.driverId = foundQuote[0].driverId;
      request.selectedQuoteId = quoteId;
      request.status = 'Assigned';

      await request.save();

      res.status(200).json({msg: 'Quote Accepted', request:request});
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

// @route POST api/customer/relocate/quotes/reject/:requestId/:quoteId
// @desc Reject a Quote
// @access Customer
router.post(
  '/reject/:requestId/:quoteId',
  [auth, isOwnerAndCustomer, isRequestAvailable],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const request = req.request;

      const { quoteId } = req.params;

      const quotes = await request.quotes.filter((el) => el._id != quoteId);

      request.quotes = quotes;

      await request.save();

      res.json(quotes);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

module.exports = router;
