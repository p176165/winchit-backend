const express = require('express');
const router = express.Router();
const randomString = require('randomstring');

const Request = require('../../../../../models/Request');
const auth = require('../../../../../middleware/auth');

const { check, validationResult } = require('express-validator');

const isCustomer = require('../../../../../middleware/isCustomer');

// @route GET api/requests/breakdown/
// @desc Get All Self Breakdown Requests
// @access Customer
// router.get('/',[auth,isCustomer],async (req,res)=>{

// })

// @route POST api/requests/breakdown/new
// @desc Create a breakdown Request
// @access Customer
// router.post(
//   '/new',
//   [
//     auth,
//     isCustomer,
//     [
//       check('breakdownType', 'Breakdown type is required').not().isEmpty(),
//       check(
//         'numberOfPassengers',
//         'Number of passengers is required'
//       ).isNumeric(),
//       check(
//         'pickupLocation.coordinates',
//         'Pickup location is wrong'
//       ).isLatLong(),
//       check(
//         'dropoffLocation.coordinates',
//         'Dropoff location is wrong'
//       ).isLatLong(),
//     ],
//   ],
//   async (req, res) => {
//     const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//       return res.status(400).json({ errors: errors.array() });
//     }

//     try {
//       const pickupCoordinates = [
//         req.body.pickupLocation.coordinates.split(',')[1],
//         req.body.pickupLocation.coordinates.split(',')[0],
//       ];
//       const dropoffCoordinate = [
//         req.body.dropoffLocation.coordinates.split(',')[1],
//         req.body.dropoffLocation.coordinates.split(',')[0],
//       ];

//       const pickupLocation = {
//         type: 'Point',
//         coordinates: pickupCoordinates,
//       };
//       const dropoffLocation = {
//         type: 'Point',
//         coordinates: dropoffCoordinate,
//       };
//       const pin = randomString.generate({ length: 4, charset: 'numeric' });
//       const customer = req.user._id;
//       const numberOfPassengers = req.body.numberOfPassengers;
//       const breakdownType = req.body.breakdownType;
//       const requestType = 'breakdown';

//       const {
//         pickupAddress,
//         pickupCity,
//         dropoffAddress,
//         dropoffCity,
//       } = req.body;

//       const pickup = {
//         location: pickupLocation,
//         address: pickupAddress,
//         city: pickupCity,
//       };
//       const dropoff = {
//         location: dropoffLocation,
//         address: dropoffAddress,
//         city: dropoffCity,
//       };

//       let request = new Request({
//         breakdownType,
//         requestType,
//         numberOfPassengers,
//         customer,
//         dropoff,
//         pickup,
//         pin,
//       });
//       await request.save();

//       res.json('Request has been saved');
//     } catch (err) {
//       console.error(err.message);
//       res.status(500).send('Server Error');
//     }
//   }
// );

module.exports = router;
