// const express = require('express');
// const router = express.Router();

// const auth = require('../../../../../middleware/auth');
// const User = require('../../../../../models/User');

// const { check, validationResult } = require('express-validator');

// const isOwnerAndCustomer = require('../../../../../middleware/isOwnerAndCustomer');
// const isRequestAvailable = require('../../../../../middleware/isRequestAvailable');

// // @route POST api/requests/breakdown/select/:requestId/:driverId
// // @desc Select a Request
// // @access Customer
// router.post(
//   '/select/:requestId/:driverId',
//   [auth, isOwnerAndCustomer, isRequestAvailable],
//   async (req, res) => {
//     const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//       return res.status(400).json({ errors: errors.array() });
//     }

//     try {
//       const request = req.request;
//       const driverId = req.params.driverId;
//       // Find driverId from Quote
//       let foundQuote = await request.quotes.filter(
//         (el) => el.driverId == req.params.driverId
//       );

//       if (foundQuote.length < 1) {
//         return res.json({ msg: 'Quote Not Found' });
//       }
//       const driver = await User.findById(driverId);
//       driver.status = 'busy';
//       request.driverId = foundQuote[0].driverId;
//       request.status = 'Assigned';
//       await driver.save();
//       await request.save();

//       var io = req.app.io;
//       io.to('Driver' + driverId).emit('quoteAccepted', { id: request._id });

//       res.json(request);
//     } catch (err) {
//       console.error(err.message);
//       res.status(500).send('Server Error');
//     }
//   }
// );

// module.exports = router;
