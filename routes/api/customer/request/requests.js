const express = require('express');
const router = express.Router();

const Request = require('../../../../models/Request');
const auth = require('../../../../middleware/auth');

const isOwnerAndCustomer = require('../../../../middleware/isOwnerAndCustomer');
const isCustomer = require('../../../../middleware/isCustomer');

// @route GET api/requests/customer/self
// @desc Get All Requests made by Customer
// @access Customer
router.get('/self', auth, isCustomer, async (req, res) => {
  try {
    const request = await Request.find({
      customer: req.user.id,
    }).populate('driverId', [
      '-profile',
      '-password',
      '-wallet',
      '-otp',
      '-isVerified',
    ]);

    if (!request) {
      return res.json({ msg: 'No Requests' });
    }

    res.json(request);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

// @route GET api/requests/customer/:requestId
// @desc Request information for owner/customer
// @access Customer
router.get('/:requestId', [auth, isOwnerAndCustomer], async (req, res) => {
  try {
    return res.json(req.request);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

// @route GET api/requests/customer/getQuotes/:requestId
// @desc Get All Quotes On Request
// @access Customer
router.get(
  '/getQuotes/:requestId',
  [auth, isOwnerAndCustomer],
  async (req, res) => {
    try {
      return res.json(req.request.quotes);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

module.exports = router;
