const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const randomString = require("randomstring");
const { check, validationResult } = require("express-validator");
const bcrypt = require("bcryptjs");
const User = require("../../../models/User");
const config = require("config");
const keys = require("../../../config/keys");
const auth = require("../../../middleware/auth");
const isDriver = require("../../../middleware/isDriver");
const isAdmin = require("../../../middleware/isAdmin");
const Vehicle = require("../../../models/Vehicle");

const passport = require("passport");

//Twilio
const { twilio } = require("../../../utils/twilio");

// Cloudinary
const { cloudinary } = require("../../../utils/cloudinary");

// @route POST api/customer/users/register
// @desc Register Customer
// @access Public

router.post(
  "/register",
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const email = req.body.email;
    const phone = req.body.phone;
    const method = "local";
    console.log(req.body);
    try {
      // Check to see if user exists
      let user = await User.findOne({ phone, role: "customer" });

      if (user) {
        return res
          .status(400)
          .json({ errors: [{ msg: "User with this phone already exists" }] });
      }

      const firstName = req.body.firstName;
      const surName = req.body.surName;
      const password = req.body.password;
      const otp = {
        code: randomString.generate({ length: 4, charset: "numeric" }),
        lastSent: Date.now(),
      };

      const role = "customer";
      const image = req.body.avatar;

      const profile = {
        customer: {
         // vehicle: vehicle,
        },
      };

      // Sets default avatar for everyone
      let avatar =
        "https://res.cloudinary.com/dogufahvv/image/upload/default.jpg";

      user = new User({
        firstName,
        surName,
        email,
        avatar,
        password,
        phone,
        otp,
        role,
        profile,
        method,
      });

      // Encrypt Password
      const salt = await bcrypt.genSalt(10);

      user.password = await bcrypt.hash(password, salt);

      if (image) {
        let url = "data:image/jpeg;base64," + image;
        const result = await cloudinary.uploader.upload(url, {
          public_id: user.id,
        });
        user.avatar = result.url;
      }
      if (req.body.registerationToken) {
        user.registerationTokens = [req.body.registerationToken];
      }
      await user.save();

      const payload = {
        user: {
          id: user.id,
        },
      };

      const vehicleData = {
        user: user.id,
        registration: req.body.vehicleRegistration,
        make: req.body.vehicleMake,
      };

      const vehicle = new Vehicle(vehicleData)
      await vehicle.save();

      // Return JsonwebToken
      jwt.sign(
        payload,
        config.get("jwtToken"),
        { expiresIn: config.get("jwtExpireTime") },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
          twilio.messages
            .create({
              body: "Your OTP for WINCHIT is " + otp.code,
              from: "+14232440608",
              to: req.body.phone,
            })
            .then((message) => {
              console.log(message.sid);
              res.send("done");
            });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

// @route POST api/customer/users/google/register
// @desc Register Customer with Google
// @access Public
router.post(
  "/google/register",
  [
    check("phone", "phone is required").not().isEmpty(),
    check("vehicleMake", "vehicleMake is required").not().isEmpty(),
    check("vehicleRegistration", "vehicleRegistration is required")
      .not()
      .isEmpty(),
  ],
  passport.authenticate("googleToken", { session: false }),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    // return res.json(req.user);
    const email = req.user.email;
    const phone = req.body.phone;
    const method = "google";
    try {
      // Check to see if user exists
      let user = await User.findOne({ phone: phone, method: method });

      if (user) {
        return res
          .status(400)
          .json({ errors: [{ msg: "User with this phone already exists" }] });
      }

      const firstName = req.user.firstName;
      const surName = req.user.surName;
      const password = "dummydata";
      const google = req.user.google;
      const otp = {
        code: randomString.generate({ length: 4, charset: "numeric" }),
        lastSent: Date.now(),
      };

      const role = "customer";
      const image = req.body.avatar;

      const vehicle = {
        registration: req.body.vehicleRegistration,
        make: req.body.vehicleMake,
      };

      const profile = {
        customer: {
          vehicle: vehicle,
        },
      };

      // Sets default avatar for everyone
      let avatar =
        "https://res.cloudinary.com/dogufahvv/image/upload/default.jpg";

      user = new User({
        firstName,
        surName,
        email,
        avatar,
        password,
        phone,
        otp,
        role,
        profile,
        method,
        google,
      });

      // Encrypt Password
      const salt = await bcrypt.genSalt(10);

      user.password = await bcrypt.hash(password, salt);

      if (image) {
        let url = "data:image/jpeg;base64," + image;
        const result = await cloudinary.uploader.upload(url, {
          public_id: user.id,
        });
        user.avatar = result.url;
      }
      if (req.body.registerationToken) {
        user.registerationTokens = [req.body.registerationToken];
      }
      await user.save();

      const payload = {
        user: {
          id: user.id,
        },
      };

      // Return JsonwebToken
      jwt.sign(
        payload,
        config.get("jwtToken"),
        { expiresIn: config.get("jwtExpireTime") },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
          twilio.messages
            .create({
              body: "Your OTP for WINCHIT is " + otp.code,
              from: "+14232440608",
              to: req.body.phone,
            })
            .then((message) => {
              console.log(message.sid);
              res.send("done");
            });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

// @route POST api/customer/users/twitter/register
// @desc Register Customer with twitter
// @access Public
router.post(
  "/twitter/register",
  [
    check("phone", "phone is required").not().isEmpty(),
    check("vehicleMake", "vehicleMake is required").not().isEmpty(),
    check("vehicleRegistration", "vehicleRegistration is required")
      .not()
      .isEmpty(),
  ],
  passport.authenticate("twitterToken", { session: false }),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    // return res.json(req.user);
    const email = req.user.email;
    const phone = req.body.phone;
    const method = "twitter";
    try {
      // Check to see if user exists
      let user = await User.findOne({ phone: phone, method: method });

      if (user) {
        return res
          .status(400)
          .json({ errors: [{ msg: "User with this phone already exists" }] });
      }

      const firstName = req.user.firstName;
      const surName = req.user.surName;
      const password = "dummydata";
      const twitter = req.user.twitter;
      const otp = {
        code: randomString.generate({ length: 4, charset: "numeric" }),
        lastSent: Date.now(),
      };

      const role = "customer";
      const image = req.body.avatar;

      const vehicle = {
        registration: req.body.vehicleRegistration,
        make: req.body.vehicleMake,
      };

      const profile = {
        customer: {
          vehicle: vehicle,
        },
      };

      // Sets default avatar for everyone
      let avatar =
        "https://res.cloudinary.com/dogufahvv/image/upload/default.jpg";

      user = new User({
        firstName,
        surName,
        email,
        avatar,
        password,
        phone,
        otp,
        role,
        profile,
        method,
        twitter,
      });

      // Encrypt Password
      const salt = await bcrypt.genSalt(10);

      user.password = await bcrypt.hash(password, salt);

      if (image) {
        let url = "data:image/jpeg;base64," + image;
        const result = await cloudinary.uploader.upload(url, {
          public_id: user.id,
        });
        user.avatar = result.url;
      }
      if (req.body.registerationToken) {
        user.registerationTokens = [req.body.registerationToken];
      }
      await user.save();

      const payload = {
        user: {
          id: user.id,
        },
      };

      // Return JsonwebToken
      jwt.sign(
        payload,
        config.get("jwtToken"),
        { expiresIn: config.get("jwtExpireTime") },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
          twilio.messages
            .create({
              body: "Your OTP for WINCHIT is " + otp.code,
              from: "+14232440608",
              to: req.body.phone,
            })
            .then((message) => {
              console.log(message.sid);
              res.send("done");
            });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

// @route POST api/customer/users/twitter/register
// @desc Register Customer with twitter
// @access Public
router.post(
  "/facebook/register",
  [
    check("phone", "phone is required").not().isEmpty(),
    check("vehicleMake", "vehicleMake is required").not().isEmpty(),
    check("vehicleRegistration", "vehicleRegistration is required")
      .not()
      .isEmpty(),
  ],
  passport.authenticate("facebookToken", { session: false }),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    // return res.json(req.user);
    const email = req.user.email;
    const phone = req.body.phone;
    const method = "facebook";
    try {
      // Check to see if user exists
      let user = await User.findOne({ phone: phone, method: method });

      if (user) {
        return res
          .status(400)
          .json({ errors: [{ msg: "User with this phone already exists" }] });
      }

      const firstName = req.user.firstName;
      const surName = req.user.surName;
      const password = "dummydata";
      const facebook = req.user.facebook;
      const otp = {
        code: randomString.generate({ length: 4, charset: "numeric" }),
        lastSent: Date.now(),
      };

      const role = "customer";
      const image = req.body.avatar;

      const vehicle = {
        registration: req.body.vehicleRegistration,
        make: req.body.vehicleMake,
      };

      const profile = {
        customer: {
          vehicle: vehicle,
        },
      };

      // Sets default avatar for everyone
      let avatar =
        "https://res.cloudinary.com/dogufahvv/image/upload/default.jpg";

      user = new User({
        facebook,
        firstName,
        surName,
        email,
        avatar,
        password,
        phone,
        otp,
        role,
        profile,
        method,
      });

      // Encrypt Password
      const salt = await bcrypt.genSalt(10);

      user.password = await bcrypt.hash(password, salt);

      if (image) {
        let url = "data:image/jpeg;base64," + image;
        const result = await cloudinary.uploader.upload(url, {
          public_id: user.id,
        });
        user.avatar = result.url;
      }
      if (req.body.registerationToken) {
        user.registerationTokens = [req.body.registerationToken];
      }
      await user.save();

      const payload = {
        user: {
          id: user.id,
        },
      };

      // Return JsonwebToken
      jwt.sign(
        payload,
        config.get("jwtToken"),
        { expiresIn: config.get("jwtExpireTime") },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
          twilio.messages
            .create({
              body: "Your OTP for WINCHIT is " + otp.code,
              from: "+14232440608",
              to: req.body.phone,
            })
            .then((message) => {
              console.log(message.sid);
              res.send("done");
            });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

router.post('/updateCustomer', auth,
  [
    check("firstName", "Firstname is required").not().isEmpty(),
    check("surName", "Surname is required").not().isEmpty(),
    check("avatar", "Avatar is required").not(),
    check("email", "Please include a valid email").isEmail()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ message: 'Some Fields Are Missing' });
    }

    try {
      const image = req.body.avatar;
      updateUser = {};
      updateUser.firstName = req.body.firstName;
      updateUser.surName = req.body.surName;
      updateUser.email = req.body.email;
      if(image) {
        let url = "data:image/jpeg;base64," + image;
        const result = await cloudinary.uploader.upload(url, {
          public_id: req.user._id,
        });
        updateUser.avatar = result.url;
      }

      await User.findByIdAndUpdate(req.user._id, updateUser, function(err) {
        if(err) return err.send('Server Error');
      });
      res.status(200).json({message: 'Updated Customer Successfully'});
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
});

router.post('/updatePhone', auth,
  [
    check("phone", "phone is required").not().isEmpty()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) return res.status(400).json({ errors: errors.array() });

    try {
      const phone = req.body.phone;
      let user = await User.findOne({ phone, role: "customer" });
      if (user) return res.status(400).json({ msg: "User with this phone already exists" });
      const otp = {
        code: randomString.generate({ length: 4, charset: "numeric" }),
        lastSent: Date.now(),
      };
      let updateUser = {};
      updateUser.otp = otp;
      updateUser.updatePhone = {
        phone: phone,
        verified: false
      };
      await User.findByIdAndUpdate(req.user._id, updateUser, function(err) {
        if(err) return err.send('Server Error');
        twilio.messages
          .create({
            body: "Your OTP for WINCHIT is " + otp.code,
            from: "+447401232103",
            to: phone,
          })
          .then((message) => {
            console.log(message.sid);
            res.status(200).json({message: 'phone number updated successfully & otp has been sent to your new phone number'});
          });
      });
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
});

module.exports = router;
