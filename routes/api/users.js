const express = require('express');
const router = express.Router();
const auth = require("../../middleware/auth");
const User = require('../../models/User');

// @route GET api/users/:id
// @desc Get info of User
// @access Both
router.get('/:id', auth, async (req, res) => {
  try {
    const user = await User.findById(req.user.id).select('-password');
    res.json(user);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

router.get("/verifyOtp/:otp", auth, function (req, res) {
  User.findOne(
    { $and: [{ _id: req.user._id }, { "otp.code": req.params.otp }] },
    function (err, foundUser) {
      if (err) {
        res.status(500).send("Invalid Link");
      } else {
        if (!foundUser || foundUser.length <= 0) {
          res.status(500).send("Wrong OTP");
        } else {
          foundUser.phone = foundUser.updatePhone.phone;
          foundUser.updatePhone = {};
          foundUser.save(function (err) {
            if (err) res.status(500).send("Some error occured");
            else res.json({ message: "account has been verified" });
          });
        }
      }
    }
  );
});

module.exports = router;
