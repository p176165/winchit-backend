const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const User = require("../../models/User");
const bcrypt = require("bcryptjs");
const randomString = require("randomstring");
const { check, validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
const config = require("config");
const keys = require("../../config/keys");
const verified = require("./../../middleware/isDriverComplete");

// // @route GET api/driver/auth/login
// // @desc Authenticate Login
// // @access Public
// router.post(
//   "/login",
//   [
//     check("phone", "Please include a valid phone number").not().isEmpty(),
//     check("password", "Please enter password").exists(),
//     check("role", "Please enter role, customer or driver").not().isEmpty(),
//   ],
//   async (req, res) => {
//     const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//       return res.status(400).json({ errors: errors.array() });
//     }

//     const { phone, password, role } = req.body;

//     if (role != "customer" && role != "driver") {
//       return res.json({ msg: "Please send role, customer or driver" });
//     }

//     try {
//       // Check to see if user exists
//       let user = await User.findOne({ phone, role });

//       if (!user) {
//         return res
//           .status(400)
//           .json({ errors: [{ msg: "Invalid Credentials" }] });
//       }

//       const isMatch = await bcrypt.compare(password, user.password);

//       if (!isMatch) {
//         return res
//           .status(400)
//           .json({ errors: [{ msg: "Invalid Credentials" }] });
//       }

//       const payload = {
//         user: {
//           id: user.id,
//         },
//       };
//       // Return JsonwebToken
//       jwt.sign(
//         payload,
//         config.get("jwtToken"),
//         { expiresIn: config.get("jwtExpireTime") },
//         async (err, token) => {
//           if (err) throw err;
//           if (req.body.registerationToken) {
//             user.registerationTokens = [req.body.registerationToken];
//             await user.save();
//           }
//           res.json({ token });
//         }
//       );
//     } catch (err) {
//       console.error(err.message);
//       res.status(500).send("Server error");
//     }
//   }
// );

module.exports = router;
