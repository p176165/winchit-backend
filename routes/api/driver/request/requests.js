const express = require('express');
const router = express.Router();
const geolib = require('geolib');
const auth = require('../../../../middleware/auth');
const { check, validationResult } = require('express-validator');
const PinRequest = require("../../../../models/PinRequest");
const Request = require("../../../../models/Request");

// @access Driver
// router.get('/:requestId', [auth, isOwnerAndCustomer], async (req, res) => {
//   try {
//     return res.json(req.request);
//   } catch (err) {
//     console.error(err.message);
//     res.status(500).send('Server Error');
//   }
// });

router.post(
  '/:requestId',
  auth,
  [
    check('reason', 'reason is required').not().isEmpty()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const request = await Request.findById(req.params.requestId);
      request.status = "onHold";
      await request.save();

      const requestData = {
        requestId: req.params.requestId,
        reason: req.body.reason
      };

      const pinRequest = new PinRequest(requestData)

      await pinRequest.save();
      res.status(200).json({ msg: 'request added' });
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

module.exports = router;

