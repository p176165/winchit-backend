const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');
const geolib = require('geolib');
const auth = require('../../../../../middleware/auth');
const Request = require('../../../../../models/Request');
const isDriver = require('../../../../../middleware/isDriver');
const isOwnerAndDriver = require('../../../../../middleware/isOwnerAndDriver');
const isRequestAssigned = require('../../../../../middleware/isRequestAssigned');
const isRequestArrived = require('../../../../../middleware/isRequestArrived');
const isRequestInProgress = require('../../../../../middleware/isRequestInProgress');

// @route GET api/driver/relocate/rides/arrived/:requestId
// @desc arrived at pickup and Change Request status
// @access Driver
router.get(
  '/arrived/:requestId',
  auth,
  isOwnerAndDriver,
  isRequestAssigned,
  async (req, res) => {
    try {
      let request = req.request;
      request.status = 'Arrived';
      //socket code
      await request.save();

      res.json({ msg: 'Status changed to arrived' });
    } catch (err) {
      console.error(err.message);
      res.json({ msg: 'Server Error' });
    }
  }
);

// @route POST api/rides/start/:requestId
// @desc Start Ride and Change Request status
// @access Driver
router.post(
  '/start/:requestId',
  auth,
  isOwnerAndDriver,
  isRequestArrived,
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      let request = req.request;
      request.status = 'InProgress';
      // socket code
      await request.save();

      res.json({ msg: 'Ride Started' });
    } catch (err) {
      console.error(err.message);
      res.json({ msg: 'Server Error' });
    }
  }
);

// @route POST api/driver/relocate/rides/end/:requestId
// @desc End Ride and Change Request status
// @access Driver
router.post(
  '/end/:requestId',
  [
    auth,
    isOwnerAndDriver,
    isRequestInProgress,
    [
      check('requestId', 'requestId is required').not().isEmpty(),
      check('pin', 'pin is required').not().isEmpty(),
    ],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { pin } = req.body;

    try {
      let request = req.request;

      if (request.pin != pin) {
        return res.json({ msg: 'Invalid PIN' });
      }

      request.status = 'Completed';
      await request.save();
      res.json({ msg: 'Ride Ended' });
    } catch (err) {
      console.error(err.message);
      res.json({ msg: 'Server Error' });
    }
  }
);

// @route GET api/driver/relocate/rides/
// @desc Get All Requests
// @access Driver
router.get('/', auth, isDriver, async (req, res) => {
  try {
    const request = await Request.find({requestType: 'scheduled', status: 'Available'}, '-quotes').populate('customer', [
      '-password',
    ]);

    if (!request) {
      return res.json({ msg: 'No Requests' });
    }

    res.json(request);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

// @route GET api/reqeusts/driver/self
// @desc Get All Requests assigned to driver
// @access Driver
router.get('/driver/self', auth, isDriver, async (req, res) => {
  try {
    const request = await Request.find({ driverId: req.user.id }, [
      '-pin',
      '-quotes',
    ]).populate('customer', [
      '-profile',
      '-password',
      '-wallet',
      '-otp',
      '-isVerified',
    ]);

    if (!request) {
      return res.json({ msg: 'No Requests' });
    }

    res.json(request);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

// @route GET api/driver/relocate/rides/:requestId
// @desc Request information for owner/Driver
// @access driver
router.get('/:requestId', [auth, isDriver], async (req, res) => {
  try {
    let request = await Request.findById(req.params.requestId, [
      '-pin',
      '-quotes',
    ]).populate('customer', [
      'avatar',
      'profile.customer.vehicle',
      'surName',
      'firstName',
    ]);
    if (!request) {
      return res.json({ msg: 'No Request found' });
    }
    return res.json(request);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

router.get('/filter/date/:date1/:date2', auth, isDriver, async (req, res) => {
  try {
    const request = await Request.find(
      {
        requestType: 'relocate',
        status: 'Available',
        dropoffDate: {
          $gte: new Date(req.params.date1),
          $lte: new Date(req.params.date2),
        },
      },
      '-quotes'
    ).populate('customer', ['-profile', '-password']);

    if (!request) {
      return res.json({ msg: 'No Requests' });
    }

    res.json(request);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

router.post(
  '/filter/pickup/',
  [
    auth,
    isDriver,
    [
      check('coordinates', 'Latitude / longitude are invalid').isLatLong(),
      check('radius', 'Radius is invalid').isInt(),
    ],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    // return res.json(req.body.coordinates)
    const { radius, coordinates } = req.body;
    try {
      const lat = coordinates.split(',')[0];
      const long = coordinates.split(',')[1];

      const request = await Request.find({
        requestType: 'relocate',
        status: 'Available',
        'pickup.location': {
          $near: {
            $geometry: {
              type: 'Point',
              coordinates: [parseFloat(long), parseFloat(lat)],
            },
            $maxDistance: radius,
          },
        },
      });

      if (!request) {
        return res.json({ msg: 'No Requests' });
      }

      res.json(request);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

router.post(
  '/filter/dropoff',
  [
    auth,
    isDriver,
    [
      check('coordinates', 'Latitude / longitude are invalid').isLatLong(),
      check('radius', 'Radius is invalid').isInt(),
    ],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    // return res.json(req.body.coordinates)
    const { radius, coordinates } = req.body;
    try {
      const lat = coordinates.split(',')[0];
      const long = coordinates.split(',')[1];

      const request = await Request.find({
        requestType: 'relocate',
        status: 'Available',
        'dropoff.location': {
          $near: {
            $geometry: {
              type: 'Point',
              coordinates: [parseFloat(long), parseFloat(lat)],
            },
            $maxDistance: radius,
          },
        },
      });

      if (!request) {
        return res.json({ msg: 'No Requests' });
      }

      res.json(request);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

router.post('/filterRequestsByDistance',
  auth,
  [
    check('location', 'location are required').not().isEmpty(),
    check('miles', 'miles are required').not().isEmpty()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) return res.status(400).json({ errors: errors.array() });

    try {
      const coordinates = [
        req.body.location.coordinates.split(',')[1],
        req.body.location.coordinates.split(',')[0],
      ];
      let requests = await Request.find({ status: 'Available', requestType: 'scheduled', driverId: { $eq: null} }).sort({createdAt: -1}).populate('customer').populate('driverId');
      const filteredRequests = [];
      requests.forEach(request=>{
        if(request.pickupKeys.address == request.pickup.address) {
          let d = geolib.getPreciseDistance(
            { latitude: coordinates[0], longitude: coordinates[1] },
            { latitude: request.pickup.location.coordinates[0], longitude: request.pickup.location.coordinates[1] }
          );
          let distance = parseFloat(d)*parseFloat(0.000621);
          request.distance = distance;
        } else {
          let d = geolib.getPreciseDistance(
            { latitude: coordinates[0], longitude: coordinates[1] },
            { latitude: request.pickupKeys.location.coordinates[0], longitude: request.pickupKeys.location.coordinates[1] }
          );
          let distance = parseFloat(d)*parseFloat(0.000621);
          request.distance = distance;
        }

        if(request.distance <= req.body.miles) {
          filteredRequests.push(request);
        }
      })

      res.status(200).json({ msg: 'filtered requests', data: filteredRequests });
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

module.exports = router;
