// const express = require('express');
// const router = express.Router();
// const randomString = require('randomstring');
// const { check, validationResult } = require('express-validator');

// const Request = require('../../../../../models/Request');

// const auth = require('../../../../../middleware/auth');
// const isOwnerAndCustomer = require('../../../../../middleware/isOwnerAndCustomer');
// const isDriver = require('../../../../../middleware/isDriver');
// const isRequestAvailable = require('../../../../../middleware/isRequestAvailable');
// const isRequestAvailableOrAssigned = require('../../../../../middleware/isRequestAvailableOrAssigned');
// const isComplete = require('../../../../../middleware/isDriverComplete');

// // @route POST api/driver/breakdown/quotes/
// // @desc Get All Quotes on Request
// // @access Driver
// router.post(
//   '/',
//   [
//     auth,
//     isComplete,
//     [check('requestId', 'requestId is required').not().isEmpty()],
//   ],
//   async (req, res) => {
//     const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//       return res.status(400).json({ errors: errors.array() });
//     }
//     const { requestId } = req.body;

//     try {
//       const request = await Request.findById(requestId).populate({
//         path: 'quotes',
//         populate: {
//           path: 'driverId',
//           model: 'user',
//           select: ['name', 'avatar'],
//         },
//       });

//       if (!request || request.customer != req.user.id) {
//         return res.json({ msg: 'No Request found' });
//       }
//       res.json(request.quotes);
//     } catch (err) {
//       console.log(err.message);
//       res.json({ msg: 'Server Error' });
//     }
//   }
// );

// // @route POST api/driver/breakdown/quotes/new/:requestId
// // @desc Post New Quote on Breakdown Request
// // @access Driver
// router.post(
//   '/new/:requestId',
//   [
//     auth,
//     isComplete,
//     isDriver,
//     isRequestAvailable,
//     [
//       check('price', 'price is required').not().isEmpty(),
//       check('ETA', 'ETA is required').not().isEmpty(),
//     ],
//   ],
//   async (req, res) => {
//     const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//       return res.status(400).json({ errors: errors.array() });
//     }
//     const { price, ETA } = req.body;

//     try {
//       let request = req.request;
//       var io = req.app.io;
//       const quotesOnRequest = request.quotes;

//       if (quotesOnRequest.some((el) => el.driverId == req.user.id)) {
//         return res.json({ msg: 'You have already placed a Quote' });
//       }

//       const quoteObject = {};
//       quoteObject.driverId = req.user.id;
//       quoteObject.price = price;
//       quoteObject.ETA = {
//         hours: ETA.hours,
//         mins: ETA.mins,
//       };

//       request.quotes.push(quoteObject);
//       await request.save();
//       io.to('Request' + request._id).emit('addQuote', quoteObject);
//       return res.json({ msg: 'Quote sent to the customer' });
//     } catch (err) {
//       console.error(err.message);
//       res.status(500).send('Server Error');
//     }
//   }
// );

// // Do not use this
// // @route POST api/driver/breakdown/quotes/cancel/:requestId
// // @desc Remove Quote from Request
// // @access Driver
// router.post(
//   '/cancel/:requestId',
//   [auth, isDriver, isRequestAvailable],
//   async (req, res) => {
//     const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//       return res.status(400).json({ errors: errors.array() });
//     }
//     const { price, ETA } = req.body;

//     try {
//       let request = req.request;
//       const quotesOnRequest = request.quotes;

//       request.quotes = quotesOnRequest.filter(
//         (el) => el.driverId != req.user.id
//       );

//       await request.save();
//       return res.json({ msg: 'Quote has been cancelled' });
//     } catch (err) {
//       console.error(err.message);
//       res.status(500).send('Server Error');
//     }
//   }
// );

// module.exports = router;
