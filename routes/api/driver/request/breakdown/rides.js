// const express = require('express');
// const router = express.Router();
// const { check, validationResult } = require('express-validator');
// const auth = require('../../../../../middleware/auth');
// const Request = require('../../../../../models/Request');

// const randomString = require('randomstring');
// const isDriver = require('../../../../../middleware/isDriver');
// const isOwnerAndDriver = require('../../../../../middleware/isOwnerAndDriver');
// const isRequestAssigned = require('../../../../../middleware/isRequestAssigned');
// const isRequestArrived = require('../../../../../middleware/isRequestArrived');
// const isRequestInProgress = require('../../../../../middleware/isRequestInProgress');
// const isComplete = require('../../../../../middleware/isDriverComplete');

// // @route GET api/driver/breakdown/rides/:requestId
// // @desc Request information for owner/Driver
// // @access driver
// router.get('/:requestId', [auth, isComplete, isDriver], async (req, res) => {
//   try {
//     let request = await Request.findById(req.params.requestId, [
//       '-pin',
//       '-quotes',
//     ]).populate('customer', [
//       'avatar',
//       'profile.customer.vehicle',
//       'surName',
//       'firstName',
//     ]);
//     if (!request) {
//       return res.json({ msg: 'No Request found' });
//     }
//     return res.json(request);
//   } catch (err) {
//     console.error(err.message);
//     res.status(500).send('Server Error');
//   }
// });

// // @route GET api/driver/breakdown/rides/arrived/:requestId
// // @desc arrived at pickup and Change Request status
// // @access Driver
// router.get(
//   '/arrived/:requestId',
//   auth,
//   isOwnerAndDriver,
//   isComplete,
//   isRequestAssigned,
//   async (req, res) => {
//     try {
//       let request = req.request;
//       request.status = 'Arrived';
//       //socket code
//       await request.save();
//       var io = req.app.io;
//       io.to('Request' + request._id).emit('riderArrived', { id: request._id });

//       res.json({ msg: 'Status changed to arrived' });
//     } catch (err) {
//       console.error(err.message);
//       res.json({ msg: 'Server Error' });
//     }
//   }
// );

// // @route GET api/driver/breakdown/rides/start/:requestId
// // @desc Start Ride and Change Request status
// // @access Driver
// router.get(
//   '/start/:requestId',
//   auth,
//   isOwnerAndDriver,
//   isComplete,
//   isRequestArrived,
//   async (req, res) => {
//     const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//       return res.status(400).json({ errors: errors.array() });
//     }

//     try {
//       let request = req.request;
//       request.status = 'InProgress';
//       // socket code
//       await request.save();
//       var io = req.app.io;
//       io.to('Request' + request._id).emit('rideStarted', { id: request._id });
//       res.json({ msg: 'Ride Started' });
//     } catch (err) {
//       console.error(err.message);
//       res.json({ msg: 'Server Error' });
//     }
//   }
// );

// // @route POST api/driver/breakdown/rides/end/:requestId
// // @desc End Ride and Change Request status
// // @access Driver
// router.post(
//   '/end/:requestId',
//   [
//     auth,
//     isOwnerAndDriver,
//     isComplete,
//     isRequestInProgress,
//     [
//       check('requestId', 'requestId is required').not().isEmpty(),
//       check('pin', 'pin is required').not().isEmpty(),
//     ],
//   ],
//   async (req, res) => {
//     const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//       return res.status(400).json({ errors: errors.array() });
//     }

//     const { pin } = req.body;

//     try {
//       let request = req.request;

//       if (request.pin != pin) {
//         return res.json({ msg: 'Invalid PIN' });
//       }

//       request.status = 'Completed';
//       await request.save();
//       var io = req.app.io;
//       io.to('Request' + request._id).emit('rideEnded', { id: request._id });
//       res.json({ msg: 'Ride Ended' });
//     } catch (err) {
//       console.error(err.message);
//       res.json({ msg: 'Server Error' });
//     }
//   }
// );

// module.exports = router;
