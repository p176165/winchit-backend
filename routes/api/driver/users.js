const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const randomString = require("randomstring");
const { check, validationResult } = require("express-validator");
const bcrypt = require("bcryptjs");
const User = require("../../../models/User");
const config = require("config");
const auth = require("../../../middleware/auth");
const Vehicle = require("../../../models/Vehicle");

//Twilio
const { twilio } = require("../../../utils/twilio");

// Cloudinary
const { cloudinary } = require("../../../utils/cloudinary");

// @route POST api/driver/users/register
// @desc Register Driver
// @access Public
router.post(
  "/register",
  [
    check("firstName", "Firstname is required").not().isEmpty(),
    check("surName", "Surname is required").not().isEmpty(),
    check("email", "Please include a valid email").isEmail(),
    check("password","Please enter a password with 6 or more characters").isLength({ min: 6 }),
    check("businessName", "businessName is required").not().isEmpty(),
    check("businessAddress", "businessAddress is required").not().isEmpty(),
    check("city", "city is required").not().isEmpty(),
    check("postCode", "postcode is required").not().isEmpty(),
    check("yearsOfExperience", "yearOfExperience is required").not().isEmpty(),
    check("phone", "phone is required").not().isEmpty(),
    check("vehicleType", "vehicleType is required").not().isEmpty(),
    check("vehicleMake", "vehicleMake is required").not().isEmpty(),
    check("vehicleRegistration", "vehicleRegistration is required").not().isEmpty(),
    check("vehicleMaxPassengers", "vehicleMaxPassengers is required").not().isEmpty(),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) return res.status(400).json({ errors: errors.array() });

    const email = req.body.email;
    const phone = req.body.phone;
    try {
      // Check to see if user exists
      let user = await User.findOne({ phone, role: "driver" });
      if (user) return res.status(400).json({ errors: [{ msg: "User with this phone already exists" }] });

      const firstName = req.body.firstName;
      const surName = req.body.surName;
      const password = req.body.password;
      const yearsOfExperience = req.body.yearsOfExperience;
      const method = "local";
      const otp = {
        code: randomString.generate({ length: 4, charset: "numeric" }),
        lastSent: Date.now(),
      };
      const role = "driver";
      const image = req.body.avatar;

      const business = {
        businessName: req.body.businessName,
        businessAddress: req.body.businessAddress,
        city: req.body.city,
        postCode: req.body.postCode,
      };

      const profile = {
        driver: {
          business: business
        },
      };
      // Sets default avatar for everyone
      let avatar =
        "https://res.cloudinary.com/dogufahvv/image/upload/default.jpg";

      user = new User({
        firstName,
        method,
        surName,
        email,
        avatar,
        password,
        phone,
        yearsOfExperience,
        otp,
        role,
        profile,
      });

      // Encrypt Password
      const salt = await bcrypt.genSalt(10);

      user.password = await bcrypt.hash(password, salt);

      if (image) {
        let url = "data:image/jpeg;base64," + image;
        const result = await cloudinary.uploader.upload(url, {
          public_id: user.id,
        });
        user.avatar = result.url;
      }
      if (req.body.registerationToken) {
        user.registerationTokens = [req.body.registerationToken];
      }
      await user.save();

      const payload = {
        user: {
          id: user.id,
        },
      };

      const vehicleData = {
        user: user.id,
        type: req.body.vehicleType,
        registration: req.body.vehicleRegistration,
        make: req.body.vehicleMake,
        maxPassengers: req.body.vehicleMaxPassengers
      };

      const vehicle = new Vehicle(vehicleData)
      await vehicle.save();

      // Return JsonwebToken
      jwt.sign(
        payload,
        config.get("jwtToken"),
        { expiresIn: config.get("jwtExpireTime") },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
          twilio.messages
            .create({
              body: "Your OTP for WINCHIT is " + otp.code,
              from: "+14232440608",
              to: req.body.phone,
            })
            .then((message) => {
              console.log(message.sid);
              res.send("done");
            });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

router.post('/changeDriverStatus/:id', auth, async (req, res) => {
	try {
    	  await User.findByIdAndUpdate(req.params.id, {"status": req.body.status}, function (err, result) {
	  	if(err) {
			res.status(400).json({error: err});
		} else {
			res.status(200).json({msg: 'status changed successfully', user: result});
		}
	  });
	} catch (err) {
	  console.error(err.message);
	  res.status(500).send('Server Error');
	}
});

router.post('/updateDriver', auth,
  [
    check("firstName", "Firstname is required").not().isEmpty(),
    check("avatar", "Avatar is required").not(),
    check("surName", "Surname is required").not().isEmpty(),
    check("email", "Please include a valid email").isEmail(),
    check("businessName", "businessName is required").not().isEmpty()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ message: 'Some Fields Are Missing' });
    }

    try {
      const image = req.body.avatar;
      updateUser = {};
      updateUser.firstName = req.body.firstName;
      updateUser.surName = req.body.surName;
      updateUser.email = req.body.email;
      // updateUser.yearsOfExperience = req.body.yearsOfExperience;
      updateUser['profile.driver.business.businessName']= req.body.businessName;
      // updateUser['profile.driver.business.businessAddress'] = req.body.businessAddress;
      // updateUser['profile.driver.business.city'] = req.body.businessCity;
      // updateUser['profile.driver.business.postCode'] = req.body.businessPostalCode;
      // updateUser['rating.currentRating'] = req.body.currentRating;
      // updateUser['rating.numberOfRating'] = req.body.numberOfRatings;
      // updateUser['wallet.balance'] = req.body.walletBalance;
      // updateUser['profile.driver.license.licenseNumber'] = req.body.licenseNumber;
      // updateUser['profile.driver.license.expiryDate'] = req.body.licenseExpiry;
      // updateUser['profile.driver.insurance.expiryDate'] = req.body.insuranceExpiry;
      if(image) {
        let url = "data:image/jpeg;base64," + image;
        const result = await cloudinary.uploader.upload(url, {
          public_id: req.user._id,
        });
        updateUser.avatar = result.url;
      }

      await User.findByIdAndUpdate(req.user._id, updateUser, function(err) {
        if(err) return err.send('Server Error');
      });
      res.status(200).json({message: 'Updated Driver Successfully'});
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
});

router.post('/updatePhone', auth,
  [
    check("phone", "phone is required").not().isEmpty()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) return res.status(400).json({ errors: errors.array() });

    try {
      const phone = req.body.phone;
      let user = await User.findOne({ phone, role: "driver" });
      if (user) return res.status(400).json({ msg: "User with this phone already exists" });
      const otp = {
        code: randomString.generate({ length: 4, charset: "numeric" }),
        lastSent: Date.now(),
      };
      let updateUser = {};
      updateUser.otp = otp;
      updateUser.updatePhone = {
        phone: phone,
        verified: false
      };
      await User.findByIdAndUpdate(req.user._id, updateUser, function(err) {
        if(err) return err.send('Server Error');
        twilio.messages
          .create({
            body: "Your OTP for WINCHIT is " + otp.code,
            from: "+447401232103",
            to: phone,
          })
          .then((message) => {
            console.log(message.sid);
            res.status(200).json({message: 'phone number updated successfully & otp has been sent to your new phone number'});
          });
      });
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
});

module.exports = router;
