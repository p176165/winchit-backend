const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');

const User = require('../../../models/User');

const auth = require('../../../middleware/auth');
const isRequestCompleted = require('../../../middleware/isRequestCompleted');
const isOwnerAndDriver = require('../../../middleware/isOwnerAndDriver');

// @route POST api/driver/ratings/:requestId/
// @desc Driver set Rating of Customer
// @access Driver
router.post(
  '/:requestId',
  [
    auth,
    isOwnerAndDriver,
    isRequestCompleted,
    [check('rating', 'Enter Valid Rating').isInt({ min: 1, max: 5 })],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      let request = req.request;

      const { rating, comment } = req.body;

      let customer = await User.findById(request.customer);

      if (request.feedbackByDriver.rating) {
        // Update old feedback

        customer.rating.currentRating =
          (customer.rating.currentRating * customer.rating.numberOfRating -
            request.feedbackByDriver.rating +
            rating) /
          customer.rating.numberOfRating;
      } else {
        // New feedback
        customer.rating.currentRating =
          (customer.rating.currentRating * customer.rating.numberOfRating +
            rating) /
          (customer.rating.numberOfRating + 1);

        customer.rating.numberOfRating = customer.rating.numberOfRating + 1;
      }

      request.feedbackByDriver.rating = rating;
      if (comment != null) {
        request.feedbackByDriver.comment = comment;
      }

      await customer.save();
      await request.save();

      res.json({ msg: 'Customer Rated' });
    } catch (err) {
      console.error(err.message);
      res.json({ msg: 'Server Error' });
    }
  }
);

module.exports = router;
