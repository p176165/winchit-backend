const express = require('express');
const router = express.Router();
const auth = require('../../../middleware/auth');

const User = require('../../../models/User');
const { check, validationResult } = require('express-validator');

const { cloudinary } = require('./../../../utils/cloudinary');

// @route POST api/driver/profile/license
// @desc Add / Update license details
// @access Driver
router.post(
  '/license',
  [
    auth,
    [
      check('image', 'Image is required').not().isEmpty(),
      check('expiryDate', 'Expiry Date is required').not().isEmpty(),
      check('licenseNumber', 'License Number is required').not().isEmpty(),
    ],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      let user = await User.findById(req.user._id);
      const image = req.body.image;
      let url = 'data:image/jpeg;base64,' + image;
      const result = await cloudinary.uploader.upload(url, {
        public_id: 'license' + req.user.id,
      });
      const license = {
        image: result.url,
        expiryDate: req.body.expiryDate,
        licenseNumber: req.body.licenseNumber,
        isVerified: true,
      };
      user.profile.driver.license = license;

      await user.save();
      res.json({ msg: 'Uploaded', data: license });
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

router.post(
  '/updateLicense',
  [
    auth,
    [
      check('image', 'Image is required').not(),
      check('expiryDate', 'Expiry Date is required').not().isEmpty(),
      check('licenseNumber', 'License Number is required').not().isEmpty(),
    ],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      let user = await User.findById(req.user._id);
      const license = {};
      const image = req.body.image;
      license.image = user.profile.driver.license.image;
      if(image != null) {
        let url = 'data:image/jpeg;base64,' + image;
        const result = await cloudinary.uploader.upload(url, {
          public_id: 'license' + req.user._id,
        });
        license.image = result.url
      }
      
      license.expiryDate= req.body.expiryDate;
      license.licenseNumber= req.body.licenseNumber;
      license.isVerified= true;
      user.profile.driver.license = license;

      await user.save();
      res.json({ msg: 'license updated successfully', data: license });
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

// @route POST api/driver/profile/insurance
// @desc Add / Update insurance details
// @access Driver
router.post(
  '/insurance',
  [
    auth,
    [
      check('image', 'Image is required').not().isEmpty(),
      check('expiryDate', 'Expiry Date is required').not().isEmpty(),
      check(
        'insuranceCoverage',
        'Please select any atleast one option from Insurance coverage'
      )
        .not()
        .isEmpty(),
    ],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      let user = await User.findById(req.user._id);
      const image = req.body.image;
      let url = 'data:image/jpeg;base64,' + image;
      const result = await cloudinary.uploader.upload(url, {
        public_id: 'insurance' + req.user.id,
      });
      const insurance = {
        image: result.url,
        expiryDate: req.body.expiryDate,
        insuranceCoverage: req.body.insuranceCoverage,
        isVerified: true,
      };
      user.profile.driver.insurance = insurance;

      await user.save();
      res.json({ msg: 'Uploaded', data: insurance });
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

router.post(
  '/updateInsurance',
  [
    auth,
    [
      check('image', 'Image is required').not(),
      check('expiryDate', 'Expiry Date is required').not().isEmpty(),
      check(
        'insuranceCoverage',
        'Please select any atleast one option from Insurance coverage'
      )
        .not()
        .isEmpty(),
    ],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      let user = await User.findById(req.user._id);
      const insurance = {};
      const image = req.body.image;
      insurance.image = user.profile.driver.insurance.image;
      if(image != null) {
        let url = 'data:image/jpeg;base64,' + image;
        const result = await cloudinary.uploader.upload(url, {
          public_id: 'insurance' + req.user._id,
        });
        insurance.image = result.url;
      }
      
      insurance.expiryDate= req.body.expiryDate;
      insurance.insuranceCoverage= req.body.insuranceCoverage;
      insurance.isVerified= true;
      user.profile.driver.insurance = insurance;

      await user.save();
      res.json({ msg: 'insurance updated', data: insurance });
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

// @route GET api/driver/profile/user/:_id
// @desc Get profile by ID
// @access Public
router.get('/user/:_id', [auth], async (req, res) => {
  try {
    const user = await User.findById(req.params._id, [
      '-password',
      '-profile.insurance',
      '-profile.license',
      '-otp',
    ]);
    if (!user) {
      return res.status(400).json({ msg: 'No user Found' });
    }
    res.json(user);
  } catch (err) {
    console.error(err.message);
    if (err.kind == 'ObjectId') {
      return res.status(400).json({ msg: 'No Profile Found' });
    }
    res.status(500).send('Server Error');
  }
});

module.exports = router;
