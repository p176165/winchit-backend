const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');

const User = require('../../models/User');
const Vehicle = require('../../models/Vehicle');
const Request = require('../../models/Request');
const { check, validationResult } = require('express-validator');

// @route GET api/profile/self
// @desc Get user profile based on Token
// @access Private
router.get('/self', auth, async (req, res) => {
  try {
    const user = await User.findById(req.user._id, ['-password']);
    const vehicles = await Vehicle.find({user: req.user._id});
    if (!user) {
      return res.status(400).json({ msg: 'There is no profile!' });
    }
	  let inProgressRide = [];
    if(user.role == "driver") {
      const ride = await Request.find({ $or: [{ status: "InProgress" }, { status: "KeysPickedUp" }, {status: "vehiclePickedUp"}], driverId: req.user._id}).populate('customer');
      inProgressRide = ride;
    }
    if(user.role == "customer") {
      const ride = await Request.find({ $or: [{ status: "InProgress" }, { status: "KeysPickedUp" }, {status: "vehiclePickedUp"}], customer: req.user._id}).populate('driverId');
      inProgressRide = ride;
    }
    res.status(200).json({user: user, vehicles: vehicles, inProgressRide: inProgressRide});
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

module.exports = router;
