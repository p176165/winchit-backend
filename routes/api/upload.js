const express = require('express');
const router = express.Router();

const auth = require('../../middleware/auth');
const { cloudinary } = require('./../../utils/cloudinary');
const User = require('../../models/User');

const { check, validationResult } = require('express-validator');

// @route POST /api/upload/profile/image
// @desc Upload Profile Image
// @access Customer or Driver
router.post(
  '/profile/image',
  [
    auth,
    [check('image', 'base64 image required in body as image').not().isEmpty()],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const image = req.body.image;
    try {
      if (image) {
        let url = 'data:image/jpeg;base64,' + image;
        const result = await cloudinary.uploader.upload(url, {
          public_id: req.user.id,
        });

        await User.findByIdAndUpdate(req.user.id, { avatar: result.url });
        return res.json({ msg: 'Uploaded' });
      } else {
        return res.json({ msg: 'Image error' });
      }
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

module.exports = router;
