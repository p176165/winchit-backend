const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const User = require('../../models/User');
const { check, validationResult } = require('express-validator');

router.post(
	'/home/:userId',
	[
	  auth,
	],
	async (req, res) => {
	  const errors = validationResult(req);
	  if (!errors.isEmpty()) {
		return res.status(400).json({ errors: errors.array() });
	  }
  
	  try {
		const homeCoordinates = [
		  req.body.coordinates.lat,
		  req.body.coordinates.long
		];
  
		const homeLocation = {
		  type: 'Point',
		  coordinates: homeCoordinates
		};
  
		const home = {
		  location: homeLocation,
		  address: req.body.address,
		  city: req.body.city
		};

		User.findByIdAndUpdate(req.params.userId, {home:home}, function(err) {
			if(err) {
			  return err;
			}
		});
		res.json({msg: 'home location is saved'});
	  } catch (err) {
		console.error(err.message);
		res.status(500).send('Server Error');
	  }
	}
);

router.post(
	'/work/:userId',
	[
	  auth,
	],
	async (req, res) => {
	  const errors = validationResult(req);
	  if (!errors.isEmpty()) {
		return res.status(400).json({ errors: errors.array() });
	  }
  
	  try {
		const workCoordinates = [
		  req.body.coordinates.lat,
		  req.body.coordinates.long
		];
  
		const workLocation = {
		  type: 'Point',
		  coordinates: workCoordinates
		};
  
		const work = {
		  location: workLocation,
		  address: req.body.address,
		  city: req.body.city
		};

		User.findByIdAndUpdate(req.params.userId, {work:work}, function(err) {
			if(err) {
			  return err;
			}
		});
		res.json({msg: 'work location is saved'});
	  } catch (err) {
		console.error(err.message);
		res.status(500).send('Server Error');
	  }
	}
);

module.exports = router;

