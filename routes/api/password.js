const express = require('express');
const router = express.Router();
const User = require('../../models/User');
const bcrypt = require('bcryptjs');
const randomString = require('randomstring');
const { check, validationResult } = require('express-validator');
const keys = require('../../config/keys');
const auth = require('../../middleware/auth');

//Twilio
const { twilio } = require('../../utils/twilio');

// @route POST api/password/forgotpassword/otp
// @desc Send OTP to users cellphone
// @access Public
router.post(
  '/forgotpassword/otp',
  [check('phone', 'Please include a valid phone number').not().isEmpty()],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      let user = await User.findOne({ phone: req.body.phone });

      if (!user) {
        return res
          .status(400)
          .json({ errors: 'No user found with this phone number' });
      }
      twilio.messages
        .create({
          body: 'Your OTP is ' + user.otp.code,
          from: '+447401232103',
          to: user.phone,
        })
        .then((message) => {
          console.log(message.sid);
          res.json({ msg: 'OTP has been sent to your phone number' });
        });
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

// @route POST api/password/forgotpassword/change
// @desc Change password after checking OTP
// @access Public
router.post(
  '/forgotpassword/change',
  [
    check('otp', 'Please enter OTP sent to your mobile number').not().isEmpty(),
    check('phone', 'Please include a valid phone number').not().isEmpty(),
    check(
      'password',
      'Please enter a password with 6 or more characters'
    ).isLength({ min: 6 }),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      let user = await User.findOne({
        $and: [{ phone: req.body.phone }, { 'otp.code': req.body.otp }],
      });

      if (!user) {
        return res.status(400).json({ errors: 'Wrong OTP' });
      }

      const salt = await bcrypt.genSalt(10);
      user.password = await bcrypt.hash(req.body.password, salt);
      user.otp = {
        code: randomString.generate({ length: 4, charset: 'numeric' }),
        lastSent: Date.now(),
      };

      await user.save();
      res.status(200).json({ msg: 'Password has been updated' });
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

// @route POST api/password/change
// @desc Change password when logged in
// @access driver/
router.post(
  '/change',
  [
    auth,
    [
      check(
        'newPassword',
        'Please enter new password with 6 or more characters'
      ).isLength({ min: 6 }),
      check(
        'oldPassword',
        'Please enter old password with 6 or more characters'
      ).isLength({ min: 6 }),
    ],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      let user = await User.findById(req.user._id);

      const isMatch = await bcrypt.compare(req.body.oldPassword, user.password);

      if (!isMatch) {
        return res.status(400).json({ errors: [{ msg: 'Invalid Password' }] });
      }

      const salt = await bcrypt.genSalt(10);
      user.password = await bcrypt.hash(req.body.newPassword, salt);

      await user.save();
      res.status(200).json({ message: 'Password has been updated' });
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

module.exports = router;
