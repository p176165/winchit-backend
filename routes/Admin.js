const Admin = require('../models/Admin');
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config");
const { check, validationResult } = require("express-validator");
const isAdmin = require("../middleware/isAdmin");

module.exports = function (app) {
  // Admin Routes
  app.use('/api/admin/drivers', require('./api/admin/drivers'));
  app.use('/api/admin/customers', require('./api/admin/customers'));
  app.use('/api/admin/dashboardData', require('./api/admin/dashboardData'));
  app.use('/api/admin/promoCodes', require('./api/admin/promoCodes'));
  app.use('/api/admin/rides', require('./api/admin/rides'));
  app.use('/api/admin/setting', require('./api/admin/setting'));
  app.use('/api/admin/pinRequests', require('./api/admin/pinRequests'));
  app.route('/api/admin/changeBanStatus/:id').get(isAdmin, async (req, res) => {
    try {
      const user = await User.findById(req.params.id).select('-password');
      if(user.banned) {
        user.banned = false;
      } else {
        user.banned = true;
      }
      user.save();
      res.status(200).send('ban status changed');
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  });

  app.route('/api/admin/addAdmin').post(
    async (req, res) => {
      const name = req.body.name;
      const email = req.body.email;
      const password = req.body.password;
  
      try {
        let admin = new Admin({
          name,
          email,
          password
        });
  
        const salt = await bcrypt.genSalt(10);
        admin.password = await bcrypt.hash(password, salt);
        await admin.save();
        res.json({ msg: 'Admin Added' });
      } catch (err) {
        console.error(err.message);
        res.status(500).send("Server error");
      }
    }
  );
  app.route('/api/adminLogin').post(
    [
      check("email", "Please include a valid email").not().isEmpty(),
      check("password", "Please enter password").exists(),
    ],
    async (req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }
  
      const { email, password } = req.body;
  
      try {
        // Check to see if user exists
        let admin = await Admin.findOne({ email });
  
        if (!admin) {
          return res.status(400).json({
            message: "Account Doesn't Exist"
          });
        }
  
        const isMatch = await bcrypt.compare(password, admin.password);
  
        if (!isMatch) {
          return res.status(400).json({
            message: "Incorrect Password",
          });
        }
  
        const payload = {
          admin: {
            email: admin.email,
          },
        };
        // Return JsonwebToken
        jwt.sign(
          payload,
          config.get("jwtToken"),
          { expiresIn: config.get("jwtExpireTime") },
          async (err, token) => {
            if (err) throw err;
              admin.token = token;
              await admin.save();
            res.status(200).json({
              token: token 
            });
          }
        );
      } catch (err) {
        res.status(500).send("Server error");
      }
    }
  );
  // app.route('/api/admin/adminLogout').get( isAdmin,
  //   async (req, res) => {
  //     try {
  //       let admin = await Admin.find({ token: req.headers['authorization'] });
  //       console.log(admin);
  //       admin.token = "";
  //       await admin.save();
  //       res.status(200).json({message: 'logged out'});
  //     } catch (err) {
  //       console.error(err.message);
  //       res.status(500).send("Server error");
  //     }
  //   }
  // );
};


