module.exports = function (app) {
  // Customer Routes
  app.use('/api/customer/users', require('./api/customer/users'));

  // Customer Auth Routes
  app.use('/api/customer/auth', require('./api/customer/auth'));
  app.use('/api/customer/myJourneys', require('./api/customer/myJourneys.js'));

  app.use('/api/customer/vehicles', require('./api/customer/vehicles'));
  // // Breakdown
  // app.use(
  //   "/api/customer/breakdown/rides",
  //   require("./api/customer/request/breakdown/rides")
  // ); //
  // app.use(
  //   "/api/customer/breakdown/quotes",
  //   require("./api/customer/request/breakdown/quotes")
  // );

  // Relocate
  app.use(
    '/api/customer/relocate/rides',
    require('./api/customer/request/relocate/rides')
  );
  app.use(
    '/api/customer/relocate/quotes',
    require('./api/customer/request/relocate/quotes')
  );

  app.use('/api/customer/requests', require('./api/customer/request/requests'));
  app.use('/api/customer/ratings', require('./api/customer/ratings'));

  app.use('/api/customer/payment', require('./api/customer/payment'));
  app.use('/api/customer/savedPlaces', require('./api/customer/savedPlaces'));
};
