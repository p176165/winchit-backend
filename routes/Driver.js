module.exports = function (app) {
  // Driver Routes
  app.use('/api/driver/users', require('./api/driver/users'));
  app.use('/api/driver/auth', require('./api/driver/auth'));
  app.use('/api/driver/profile', require('./api/driver/profile'));
  app.use('/api/driver/vehicles', require('./api/driver/vehicles'));
  app.use('/api/driver/myBookings', require('./api/driver/myBookings.js'));
  app.use('/api/driver/pinRequest', require('./api/driver/request/requests.js'));
  // // Breakdown
  // app.use(
  //   '/api/driver/breakdown/quotes',
  //   require('./api/driver/request/breakdown/quotes')
  // ); //
  // app.use(
  //   '/api/driver/breakdown/rides',
  //   require('./api/driver/request/breakdown/rides')
  // );

  // Relocate
  app.use(
    '/api/driver/relocate/quotes',
    require('./api/driver/request/relocate/quotes')
  ); //
  app.use(
    '/api/driver/relocate/rides',
    require('./api/driver/request/relocate/rides')
  ); //
  // app.use('/api/driver/rides', require('./routes/api/driver/request/requests'));
  app.use('/api/driver/ratings', require('./api/driver/ratings'));
};
