const nodemailer = require('nodemailer');
const config = require('config');

const transporter = nodemailer.createTransport({
	service: 'gmail',
	port: 587,
    secure: false,
	auth: {
	  user: config.get('MAIL_USERNAME'),
	  pass: config.get('MAIL_PASSWORD')
	}
});

module.exports = transporter;