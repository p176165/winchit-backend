const GoogleStrategy = require("passport-google-token").Strategy;
const FacebookTokenStrategy = require("passport-facebook-token");
const TwitterTokenStrategy = require("passport-twitter-token");
const User = require("../models/User");
const keys = require("./keys");

module.exports = function (passport) {
  // Google OAuth Register Strategy
  passport.use(
    "googleToken",
    new GoogleStrategy(
      {
        clientID: keys.google.clientID,
        clientSecret: keys.google.clientSecret,
        passReqToCallback: true,
      },
      async (req, accessToken, refreshToken, profile, done) => {
        try {
          console.log(profile);
          console.log(req.body);
          let existingUser = await User.findOne({
            "google.id": profile.id,
            role: "customer",
          });
          if (existingUser) {
            return done("User already exists with this account", false, {
              message: "User already exists with this account",
            });
          }

          const newUser = {
            method: "google",
            google: {
              id: profile.id,
            },
            email: profile.emails[0].value,
            firstName: profile.name.givenName,
            surName: profile.name.familyName,
          };
          done(null, newUser);
        } catch (error) {
          done(error, false, error.message);
        }
      }
    )
  );

  //Google login
  passport.use(
    "googleTokenLogin",
    new GoogleStrategy(
      {
        clientID: keys.google.clientID,
        clientSecret: keys.google.clientSecret,
        passReqToCallback: true,
      },
      async (req, accessToken, refreshToken, profile, done) => {
        try {
          console.log(profile);
          console.log(req.body);
          let existingUser = await User.findOne({
            "google.id": profile.id,
            role: "customer",
          });
          if (existingUser) {
            return done(null, existingUser);
          } else {
            return done("No User Registered this Google acount", false, {
              message: "No User Registered this Google acount",
            });
          }
        } catch (error) {
          done(error, false, error.message);
        }
      }
    )
  );

  // Twitter Register
  passport.use(
    "twitterToken",
    new TwitterTokenStrategy(
      {
        consumerKey: keys.twitter.consumerKey,
        consumerSecret: keys.twitter.consumerSecret,
        passReqToCallback: true,
      },
      async (req, token, tokenSecret, profile, done) => {
        try {
          console.log(profile);
          console.log(req.body);
          let existingUser = await User.findOne({
            "twitter.id": profile.id,
            role: "customer",
          });
          if (existingUser) {
            return done("User already exists with this account", false, {
              message: "User already exists with this account",
            });
          }

          const newUser = {
            method: "twitter",
            twitter: {
              id: profile.id,
            },
            email: profile.emails[0].value,
            firstName: profile.displayName,
            surName: profile.username,
          };
          done(null, newUser);
        } catch (error) {
          done(error, false, error.message);
        }
      }
    )
  );

  // Twitter Auth Login
  passport.use(
    "twitterTokenLogin",
    new TwitterTokenStrategy(
      {
        consumerKey: keys.twitter.consumerKey,
        consumerSecret: keys.twitter.consumerSecret,
        passReqToCallback: true,
      },
      async (req, token, tokenSecret, profile, done) => {
        try {
          console.log(profile);
          console.log(req.body);
          let existingUser = await User.findOne({
            "twitter.id": profile.id,
            role: "customer",
          });
          if (existingUser) {
            return done(null, existingUser);
          } else {
            return done("No User Registered this Twitter acount", false, {
              message: "No User Registered this Twitter acount",
            });
          }
        } catch (error) {
          done(error, false, error.message);
        }
      }
    )
  );

  // Facebook Register
  passport.use(
    "facebookToken",
    new FacebookTokenStrategy(
      {
        clientID: keys.facebook.clientID,
        clientSecret: keys.facebook.clientSecret,
        passReqToCallback: true,
        profileFields: ["name", "email"],
      },
      async (req, accessToken, refreshToken, profile, done) => {
        try {
          console.log("profile", profile);
          console.log("accessToken", accessToken);
          console.log("refreshToken", refreshToken);

          const existingUser = await User.findOne({
            "facebook.id": profile.id,
            role: "customer",
          });
          if (existingUser) {
            return done("User already exists with this account", false, {
              message: "User already exists with this account",
            });
          }

          const newUser = {
            method: "facebook",
            facebook: {
              id: profile.id,
            },
            email: profile.emails[0].value,
            firstName: profile.name.givenName,
            surName: profile.name.familyName,
          };

          done(null, newUser);
        } catch (error) {
          done(error, false, error.message);
        }
      }
    )
  );

  // Facebook Login
  passport.use(
    "facebookTokenLogin",
    new FacebookTokenStrategy(
      {
        clientID: keys.facebook.clientID,
        clientSecret: keys.facebook.clientSecret,
        passReqToCallback: true,
      },
      async (req, accessToken, refreshToken, profile, done) => {
        try {
          console.log("profile", profile);
          console.log("accessToken", accessToken);
          console.log("refreshToken", refreshToken);

          const existingUser = await User.findOne({
            "facebook.id": profile.id,
            role: "customer",
          });
          if (existingUser) {
            return done(null, existingUser);
          } else {
            return done("No User Registered this Facebook acount", false, {
              message: "No User Registered this Facebook acount",
            });
          }
        } catch (error) {
          done(error, false, error.message);
        }
      }
    )
  );
};
