const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = function (req, res, next) {
  if (
    req.user.isVerified &&
    req.user.profile.driver.license &&
    req.user.profile.driver.insurance
  ) {
    next();
  } else if (!req.user.isVerified) {
    res.status(401).json({ msg: 'User Phone not verified' });
  } else if (!req.user.profile.driver.license) {
    res.status(401).json({ msg: 'License not verified' });
  } else if (!req.user.profile.driver.insurance) {
    res.status(401).json({ msg: 'Insurance not verified' });
  } else {
    res.status(401).json({ msg: 'Contact Admin' });
  }
};
