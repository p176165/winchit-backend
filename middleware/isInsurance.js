const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = function (req, res, next) {
    if (req.user.profile.insurance ) {
        next();
    } 
    else {
        res.status(401).json({ msg: 'Insurance not verified' });
    }
}
