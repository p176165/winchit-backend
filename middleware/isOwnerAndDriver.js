const jwt = require('jsonwebtoken');
const config = require('config');
const Request = require('../models/Request');

module.exports = async function (req, res, next) {
    try {
        let request = await Request.findById(req.params.requestId,["-pin",]);
        if (!request) {
            return res.json({ msg: 'No Request found' });
        }
        if (req.user.role == 'driver' && (request.driverId && request.driverId.equals(req.user.id))) {
            req.request = request;
            next();
        } else {
            res.status(401).json({ msg: 'You are not authorised for this action' });
        }
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
}
