const Payment = require("../models/Payment");
const stripe = require("stripe")(
  "sk_test_51HQtLcLx4TQNDhPV83EKbjYg7lFRaLQNtFspqfa4fzIFdabY4EllAGzRBS5EwVo0kWsvFYEnU5hAom6rBXlr3UHb000njUmIrJ"
);

module.exports = async function (req, res, next) {
  // Get StripeId for Customer
  // if stripe id not found
  // create stripe id

  try {
    let payment = await Payment.findOne({ user: req.user.id });

    // If Payment doesn't exist
    if (!payment) {
      const customer = await stripe.customers.create({
        phone: req.user.phone,
        email: req.user.email,
        name: req.user.firstName + " " + req.user.surName,
        metadata: {
          id: "" + req.user._id + "",
        },
      });
      const stripeId = customer.id;
      const user = req.user.id;
      payment = new Payment({ stripeId, user });
      await payment.save();
    }

    // If StripeId doesn't exist
    if (!payment.stripeId) {
      // Create new Stripe ID
      const customer = await stripe.customers.create({
        phone: req.user.phone,
        email: req.user.email,
        name: req.user.firstName + " " + req.user.surName,
        metadata: {
          id: "" + req.user._id + "",
        },
      });
      payment.stripeId = customer.id;
      await payment.save();
    }

    req.stripeId = payment.stripeId;

    next();
  } catch (error) {
    return res.json({ msg: error.message });
  }
};
