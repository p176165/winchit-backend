const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = function (req, res, next) {
    if (req.user.isVerified ) {
        next();
    }
    else {
        res.status(401).json({ msg: 'Insurance not verified' });
    }
}
