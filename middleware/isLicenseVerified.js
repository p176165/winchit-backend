const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = function (req, res, next) {
    if (req.user.isVerified && (req.user.profile.license )) {
        next();
    } 
    else {
        res.status(401).json({ msg: 'License not verified' });
    }
}
