const jwt = require('jsonwebtoken');
const config = require('config');
const Request = require('../models/Request');

module.exports = async function (req, res, next) {
    try {
        let request = await Request.findById(req.params.requestId);
        if (!request) {
            return res.json({ msg: 'No Request found' });
        }
        if (request.status == "Assigned") {
            req.request = request;
            next();
        } else {
            res.status(401).json({ msg: 'Request is not assigned' });
        }
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
}
