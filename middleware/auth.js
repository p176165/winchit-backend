const jwt = require('jsonwebtoken');
const config = require('config');
const User = require('../models/User');

module.exports = async function (req, res, next) {
  // Get token from Header
  
  var token = req.header('authorization');
  console.log(token);
  var actualTokken = token.split(" ");
  token = actualTokken[1];
  console.log(token);
  // Check if no Token
  if (!token) {
    return res.status(401).json({ msg: 'No token, authorization denied' });
  }

  // Verify Token
  try {
    const decoded = jwt.verify(token, config.get('jwtToken'));
    const user = await User.findById(decoded.user.id).select('-password');
    req.user = user;
    req.user.id = user._id;
    next();
  } catch (err) {
    console.log(err);
    res.status(401).json({ msg: 'Token is not valid' });
  }
};
