const jwt = require('jsonwebtoken');
const config = require('config');
const Request = require('../models/Request');

module.exports = async function (req, res, next) {
  try {
    
    var token = req.headers['authorization'];
    token = token.split(" ");
    token = token[1];
    if (!token) return res.status(401).send({ msg: 'Unauthorized' });
    jwt.verify(token, config.get("jwtToken"), function(err, decoded) {
      if (err) return res.status(500).send({ message: 'Invalid Token' });
      next();
    });
    // if (req.user.role == 'Admin') {
    //   next();
    // } else {
    //   res.status(401).json({ msg: 'You are not authorised for this action' });
    // }
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
};
