const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = async function (req, res, next) {
  try {
    if (req.user.role == 'customer' || req.user.role == 'both') {
      next();
    } else {
      res.status(401).json({ msg: 'You are not authorised for this action' });
    }
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
};
