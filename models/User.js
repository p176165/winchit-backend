const mongoose = require('mongoose');

const CustomerVehicleSchema = require('./CustomerVehicle');
const InsuranceSchema = require('./Insurance');
const LicenseSchema = require('./License');
const DriverVehicleSchema = require('./DriverVehicle');
const BusinessSchema = require('./Business');

const ProfileSchema = new mongoose.Schema({
  // Every profile is associated to a user
  driver: {
    business: BusinessSchema,
 //   vehicle: DriverVehicleSchema,
    license: LicenseSchema,
    insurance: InsuranceSchema,
  },
  customer: {
   // vehicle: CustomerVehicleSchema,
  },
});

const UserSchema = new mongoose.Schema({
  method: {
    type: String,
    required: true,
  },
  google: {
    id: {
      type: String,
    },
  },
  facebook: {
    id: {
      type: String,
    },
  },
  twitter: {
    id: {
      type: String,
    },
  },
  firstName: {
    type: String,
    required: true,
  },
  surName: {
    type: String,
    required: true,
  },
  phone: {
    type: String,
    required: true,
  },
  updatePhone: {
    phone: {
      type: String
    },
    verified: {
      type: Boolean,
      default: false,
    },
  },
  isVerified: {
    type: Boolean,
    default: false,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  avatar: {
    type: String,
  },
  yearsOfExperience: {
    type: Number,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  rating: {
    numberOfRating: {
      type: Number,
      default: 0,
    },
    currentRating: {
      type: Number,
      default: 0,
    },
  },
  wallet: {
    balance: {
      type: Number,
      default: 0,
      required: true,
    },
  },
  otp: {
    lastSent: {
      type: Date,
      required: true,
    },
    code: {
      type: String,
      required: true,
    },
  },
  status: {
    type: String,
  },
  banned: {
    type: Boolean,
    default: false
  },
  location: {
	  type: { type: String },
	  coordinates: {type: [Number]}
  },
  role: {
    type: String,
    default: 'customer',
    required: true,
  },
  registerationTokens: {
    type: [String],
  },
  profile: ProfileSchema,
	home: {
    location: {
      type: {
        type: String,
        enum: ['Point'],
        default: 'Point',
      },
      coordinates: {
        type: [Number],
      },
    },
    address: {
      type: String
    },
    city: {
      type: String
    }
  },
  work: {
    location: {
      type: {
        type: String,
        enum: ['Point'],
        default: 'Point',
      },
      coordinates: {
        type: [Number],
      },
    },
    address: {
      type: String
    },
    city: {
      type: String
    }
  },
  is_busy: {
    type: Boolean,
    default: false
  },
  reminderForLicenseSent: {
    type: Boolean,
    default: false
  },
  reminderForInsuranceSent: {
    type: Boolean,
    default: false
  }
});

UserSchema.index({ location: '2dsphere' });

module.exports = User = mongoose.model('user', UserSchema);
