const mongoose = require('mongoose');

const VehicleSchema = new mongoose.Schema({
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'user',
		required: true
	},
	type: {
		type: String
	},
	registration: {
		type: String,
		required: true,
	},
	make: {
		type: String,
		required: true,
	},
	maxPassengers: {
		type: String
	},
});

module.exports = mongoose.model('vehicle', VehicleSchema);

