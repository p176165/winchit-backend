const mongoose = require('mongoose');

const SavedPlacesSchema = new mongoose.Schema({
	userId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'user',
		required: true
	},
	title: {
		type: String,
        required: true
	},
	location: {
    type: {
      type: String,
      enum: ['Point'],
      default: 'Point',
    },
    coordinates: {
      type: [Number]
    }
  },
  address: {
    type: String
  },
  city: {
    type: String
  }
});

module.exports = mongoose.model('saved_places', SavedPlacesSchema);

