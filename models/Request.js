const mongoose = require('mongoose');

const LocationSchema = require('./Location');

const RequestSchema = mongoose.Schema({
  customer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
  },
  driverId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
  },
  selectedQuoteId: {
    type: mongoose.Schema.Types.ObjectId,
  },
  requestType: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  breakdownType: {
    type: String,
  },
  numberOfPassengers: {
    type: Number,
  },
  pickup: LocationSchema,
  pickupKeys: LocationSchema,
  dropoff: LocationSchema,
  dropoffDate: {
    type: Date,
  },
  distanceKeysToPickup: {
    type: String,
    default: null
  },
  distancePickupToDropoff: {
    type: String,
    default: null
  },
  status: {
    type: String,
    default: 'Available',
    required: true,
  },
  pin: {
    type: String,
    required: true,
  },
  quotes: [
    {
      createdAt: {
        type: Date,
        default: Date.now,
      },
      price: {
        type: Number,
        required: true,
      },
      ETA: {
        hours: {
          type: String,
        },
        minutes: {
          type: String,
        },
        time: {
          type: Date,
        },
      },
      driverId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
      },
    },
  ],
  feedbackByDriver: {
    comment: {
      type: String,
    },
    rating: {
      type: Number,
    },
  },
  feedbackByCustomer: {
    comment: {
      type: String,
    },
    rating: {
      type: Number,
    },
    tip: {
      type: Number,
      default: 0,
    },
  },
  notes: {
    type: String,
  },
  vehicleRegistration: {
    type: String
  },
  vehicleMake: {
    type: String
  },
  isKeyAtPickupLocation: {
    type: String
  }
});

module.exports = mongoose.model('request', RequestSchema);
