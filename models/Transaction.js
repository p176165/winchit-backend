const mongoose = require('mongoose');

const TransactionSchema = new mongoose.Schema({
  request: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'request',
  },
  tip: {
    type: Number,
  },
  customerPaid: {
    type: Number,
  },
  chargeId: {
    type: String,
  },
});

module.exports = mongoose.model('transaction', TransactionSchema);
