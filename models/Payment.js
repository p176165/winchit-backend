const mongoose = require('mongoose');

const PaymentSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
  },
  stripeId: {
    type: String,
  },
});

module.exports = mongoose.model('payment', PaymentSchema);
