const mongoose = require('mongoose');

const DriverVehicleSchema = new mongoose.Schema({
  Type: {
    type: String,
    required: true,
  },
  registration: {
    type: String,
    required: true,
  },
  make: {
    type: String,
    required: true,
  },
  maxPassengers: {
    type: String,
    required: true,
  },
});
