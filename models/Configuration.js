const mongoose = require('mongoose');

const ConfigurationSchema = mongoose.Schema({
  companyCommission: {
    type: Number,
    required: true
  },
  driverLowRatingLimit: {
    type: Number,
    required: true
  },
  customerLowRatingLimit: {
    type: Number,
    required: true
  },
  driverHighRatingLimit: {
    type: Number,
    required: true
  },
  customerHighRatingLimit: {
    type: Number,
    required: true
  },
  searchRangeForDriver: {
	  type: Number,
	  required: true
  }
});

module.exports = mongoose.model('configuration', ConfigurationSchema);
