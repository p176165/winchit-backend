const mongoose = require('mongoose');

const LicenseSchema = new mongoose.Schema({
  isVerified: {
    type: Boolean,
    default: false,
  },
  image: {
    type: String,
    required: true,
  },
  licenseNumber: {
    type: String,
    required: true,
  },
  expiryDate: {
    type: Date,
    required: true,
  },
});
