const mongoose = require('mongoose');

const PromoCodeSchema = new mongoose.Schema({
	title: {
		type: String,
		required: true
	},
	code: {
		type: String,
		required: true
	},
	discount: {
		type: Number,
		required: true
	},
	percentage: {
		type: Boolean,
		required: true
	},
	start_date: {
		type: Date,
		required: true
	},
	expire_on: {
		type: Date,
		required: true
	},
	createdAt: {
		type: Date,
		default: Date.now,
	}
});

module.exports = PromoCode = mongoose.model('promoCode', PromoCodeSchema);