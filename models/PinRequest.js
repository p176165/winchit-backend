const mongoose = require('mongoose');

const PinRequestSchema = new mongoose.Schema({
  requestId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'request',
  },
  reason: {
    type: String,
	required: true
  },
  solved: {
    type: Boolean,
	default: false
  }
});

module.exports = mongoose.model('pinRequest', PinRequestSchema);

