const mongoose = require('mongoose');

const HelpSchema = mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
    required: true
  },
  subject: {
	type: String,
	required: true
  },
  message: {
	type: String,
	required: true
  }
});

module.exports = mongoose.model('help', HelpSchema);
