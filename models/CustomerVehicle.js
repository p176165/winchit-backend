const mongoose = require('mongoose');

const CustomerVehicleSchema = new mongoose.Schema({
  registration: {
    type: String,
    required: true,
  },
  make: {
    type: String,
    required: true,
  },
});
