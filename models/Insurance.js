const mongoose = require('mongoose');

const InsuranceSchema = new mongoose.Schema({
  isVerified: {
    type: Boolean,
    default: false,
  },
  image: {
    type: String,
    required: true,
  },
  expiryDate: {
    type: Date,
    required: true,
  },
  insuranceCoverage: {
    type: [String],
  },
});
