const jwt = require('jsonwebtoken');
const config = require('config');
const User = require('./models/User');
const Request = require('./models/Request');
const randomString = require('randomstring');

module.exports = function (app, io) {
  app.io = io
    .use(function (socket, next) {
      console.log('ww');
      if (socket.handshake.query && socket.handshake.query.token) {
        jwt.verify(
          socket.handshake.query.token,
          config.get('jwtToken'),
          function (err, decoded) {
            if (err) {
              console.log(err);
              return next(new Error('Authentication error'));
            }
            socket.decoded = decoded;
            console.log(decoded);
            next();
          }
        );
      } else {
        next(new Error('Authentication error'));
      }
    })
    .on('connection', function (socket) {
      console.log('socket created');

      socket.emit('connect', 'You are connected');

      socket.on('disconnect', function () {
        User.findOneAndUpdate(
          { _id: socket.decoded.user.id, role: 'driver', status: 'online' },
          { $set: { status: 'offline' } },
          function (err, updatedUser) {
            if (err) {
              console.log(
                'error on updating driver status when he disconnects'
              );
            } else {
              console.log('Got disconnected!');
            }
          }
        );
      });

      socket.on('lastKnownLocation', function (data) {
        const lastLocation = {
          type: 'Point',
          coordinates: [data.Coordinate.Longitude, data.Coordinate.Latitude],
        };

        User.findOneAndUpdate(
          { _id: socket.decoded.user.id },
          { $set: { lastLocation: lastLocation } },
          function (err, updatedUser) {
            if (err) {
              socket.emit('locationResponse', err);
            } else {
              socket.emit('locationResponse', updatedUser);
            }
          }
        );
      });

      socket.on('getOnline', function (data) {
        User.findOne({ _id: socket.decoded.user.id }, function (
          err,
          foundUser
        ) {
          if (err) {
            socket.emit('response', err);
          } else {
            // socket.emit('locationResponse', updatedUser);
            socket.join('Driver' + socket.decoded.user.id);
            socket.emit(
              'response',
              'Joined room: Driver' + socket.decoded.user.id
            );
            console.log(data);
            foundUser.status = 'online';
            foundUser.save();
          }
        });
      });

      //Request Riders after making breakdown request
      socket.on('requestRiders', function (data) {
        // return console.log(data);
        if (data.Coordinate) {
          User.find(
            {
              role: 'driver',
              status: 'online',
              lastLocation: {
                $near: {
                  $geometry: {
                    type: 'Point',
                    coordinates: [
                      parseFloat(data.Coordinate.Longitude),
                      parseFloat(data.Coordinate.Latitude),
                    ],
                  },
                  $maxDistance: 1000,
                },
              },
            },
            function (err, foundUsers) {
              if (err) {
                console.log(err);
                socket.emit('response', err);
              } else {
                if (foundUsers.length < 1) {
                  socket.emit('response', 'No drivers available right now.');
                } else {
                  // Join Request Room
                  socket.join('Request' + data.request.id);
                  socket.emit(
                    'response',
                    'Joined Room: Request' + data.request.id
                  );
                  var request = {
                    _id: data.request.id,
                  };
                  foundUsers.forEach(function (user) {
                    io.of('/driver')
                      .to('Driver' + user._id)
                      .emit('breakdownRequest', request);
                  });
                  socket.emit('response', 'Waiting for drivers to respond');
                }
              }
            }
          );
        } else {
          socket.emit(
            'response',
            'Please send Coordinate.Longitude and Coordinate.Latitude'
          );
        }
      });

      socket.on('relocateRequest', function (data) {
        // Check if user is customer or both

        if (
          !('pickupLocation' in data) ||
          !('pickupCity' in data) ||
          !('pickupAddress' in data) ||
          !('pickupKeysLocation' in data) ||
          !('pickupKeysCity' in data) ||
          !('pickupKeysAddress' in data) ||
          !('dropoffLocation' in data) ||
          !('dropoffCity' in data) ||
          !('dropoffAddress' in data) ||
          !('vehicleRegistration' in data) ||
          !('vehicleMake' in data) ||
          !('userLocation' in data)
        ) {
          console.log('error');
          socket.emit(
            'response',
            'Please send {pickupLocation: "2,4",pickupCity: "pickupCity",pickupAddress: "Address",dropoffLocation: "5,2",dropoffCity: "dropoffCity",dropoffAddress: "Address",pickupKeysLocation: "5,2",pickupKeysCity: "pickupKeysCity",pickupKeysAddress: "Address", userLocation: "userLocation",vehicleRegistration: "vehicleRegistration", vehicleMake: "vehicleMake"}'
          );
        } else {
          User.findById(socket.decoded.user.id, (err, foundUser) => {
            // Create new request in database

            if (err) {
              console.log(err);
            }

            try {
              const pickupCoordinates = [
                data.pickupLocation.split(',')[1],
                data.pickupLocation.split(',')[0],
              ];
              const dropoffCoordinate = [
                data.dropoffLocation.split(',')[1],
                data.dropoffLocation.split(',')[0],
              ];
              const pickupKeyCoordinates = [
                data.pickupKeysLocation.split(',')[1],
                data.pickupKeysLocation.split(',')[0],
              ];
              const pickupLocation = {
                type: 'Point',
                coordinates: pickupCoordinates,
              };
              const dropoffLocation = {
                type: 'Point',
                coordinates: dropoffCoordinate,
              };
              const pickupKeysLocation = {
                type: 'Point',
                coordinates: pickupKeyCoordinates,
              };
              const pin = randomString.generate({
                length: 4,
                charset: 'numeric',
              });
              const customer = foundUser._id;
              const {
                pickupAddress,
                pickupCity,
                pickupKeysAddress,
                pickupKeysCity,
                dropoffAddress,
                dropoffCity,
                userLocation,
                Notes,
                vehicleRegistration,
                vehicleMake
              } = data;

              const requestType = 'relocate';
              const pickup = {
                location: pickupLocation,
                address: pickupAddress,
                city: pickupCity,
              };
              const pickupKeys = {
                location: pickupKeysLocation,
                address: pickupKeysAddress,
                city: pickupKeysCity,
              };
              const dropoff = {
                location: dropoffLocation,
                address: dropoffAddress,
                city: dropoffCity,
              };
              let request = new Request({
                requestType,
                customer,
                dropoff: dropoff,
                pickup: pickup,
                pickupKeys: pickupKeys,
                pin,
                Notes,
                vehicleRegistration,
                vehicleMake
              });

              request.save().then(() => {
                // Send request to all nearby Drivers for Quotes
                User.find(
                  {
                    role: 'driver',
                    status: 'online',
                    lastLocation: {
                      $near: {
                        $geometry: {
                          type: 'Point',
                          coordinates: [
                            parseFloat(userLocation.longitude),
                            parseFloat(userLocation.latitude),
                          ],
                        },
                        $maxDistance: 1000,
                      },
                    },
                  },
                  function (err, nearbyDrivers) {
                    if (err) {
                      console.log(err);
                      socket.emit('response', err);
                    } else {
                      if (nearbyDrivers.length < 1) {
                        socket.emit(
                          'response',
                          'No drivers available right now.'
                        );
                      } else {
                        // Join Request Room
                        socket.join('Request' + request._id);
                        socket.emit(
                          'response',
                          'Joined Room: Request' + request._id
                        );
                        nearbyDrivers.forEach(function (user) {
                          io.to('Driver' + user._id).emit(
                            'rideRequest',
                            request
                          );
                        });
                        socket.emit(
                          'response',
                          'Waiting for drivers to respond'
                        );
                      }
                    }
                  }
                );
              });
            } catch (error) {
              socket.emit('response', error.message);
            }
          });
        }
      });

      socket.on('joinRideRoom', function (data) {
        if (!data.request.id) {
          Request.findOne(
            { _id: data.request.id, driverId: socket.decoded.user.id },
            function (err, foundRide) {
              if (err) {
                socket.emit('response', err);
              } else {
                // socket.emit('locationResponse', updatedUser);
                if (!foundRide) {
                  socket.emit('response', 'No Ride found');
                }
                socket.join('Request' + data.request.id);
                socket.emit(
                  'response',
                  'Joined room: Request' + data.request.id
                );
              }
            }
          );
        } else {
          socket.emit('response', 'Please send Request: {id: "somerequestID"}');
        }
      });
    });
};
