//Twilio Setup
const keys = require('../config/keys');

const accountSid = keys.twilio.accountSid;
const authToken = keys.twilio.authToken;
const twilio = require('twilio')(accountSid, authToken);

module.exports = { twilio };
