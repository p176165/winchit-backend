var cron = require('node-cron');
const User = require('../models/User');
const sendMail = require('./mail');

module.exports = function() {
	cron.schedule('00 00 12 * * 0-6', async () => {
		let today = new Date();
    const todayDate= today.toLocaleDateString("en-US");
		let drivers = await User.find({role: "driver"});
		for(const driver of drivers) {
			const licenseDaysLeft = getDifferenceInDays(new Date(todayDate), new Date(driver.profile.driver.license.expiryDate));
			const insuranceDaysLeft = getDifferenceInDays(new Date(todayDate), new Date(driver.profile.driver.insurance.expiryDate));
			if(licenseDaysLeft === 7 && !driver.reminderForLicenseSent) {
				let text = "Your License is going to expire on "+driver.profile.driver.license.expiryDate;
				sendMail('hashloopstech@gmail.com', driver.email, 'License Expiry Reminder', text);
				await User.findByIdAndUpdate(driver._id, {reminderForLicenseSent: true}, function(err) {
					if(err) console.log(err)
				})
			}
			if(insuranceDaysLeft === 7 && !driver.reminderForInsuranceSent) {
				let text = "Your Insurance is going to expire on "+driver.profile.driver.insurance.expiryDate;
				sendMail('hashloopstech@gmail.com', driver.email, 'Insurance Expiry Reminder', text);
				await User.findByIdAndUpdate(driver._id, {reminderForInsuranceSent: true}, function(err) {
					if(err) console.log(err)
				})
			}
		}
	});
}

function getDifferenceInDays(date1, date2) {
	const diffInMs = Math.abs(date2 - date1);
	return diffInMs / (1000 * 60 * 60 * 24);
}