const transporter = require('../config/mail');

module.exports = function (from, to, subject, text) {
	const message = {
		from: from, // Sender address
		to: to,         // List of recipients
		subject: subject, // Subject line
		html: text // Plain text body
	};
	transporter.sendMail(message, function(err, info) {
		if (err) {
		console.log(err)
		} else {
		console.log('email sent');
		}
	});
}