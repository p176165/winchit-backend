const express = require('express');
const connectDB = require('./config/db');
const bodyParser = require('body-parser');
const passport = require('passport');
const cronJob = require('./utils/cronJob');

const app = express();
var cors = require('cors');
var socket_io = require('socket.io');
var io = socket_io();

const DriverRoutes = require('./routes/Driver');
const CustomerRoutes = require('./routes/Customer');
const BothRoutes = require('./routes/Both');
const AdminRoutes = require('./routes/Admin');
const GenerateToken = require('./routes/token');

const CustomerSocket = require('./sockets/customer');
const DriverSocket = require('./sockets/driver');

// Connecting Database
connectDB();

cronJob();

app.use(express.json({ extended: false }));
app.use(cors());

app.use(bodyParser.json({ limit: '50mb', extended: true }));
app.use(
  bodyParser.urlencoded({
    limit: '50mb',
    extended: true,
    parameterLimit: 50000,
  })
);

// Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

app.get('/', (req, res) => res.send('API Running'));

app.get('/deleteUsers', function (req, res) {
  User.deleteMany({}).then(result => {
    res.json({msg: result});
  })
})

// Driver Routes
DriverRoutes(app);

// Custome Routes
CustomerRoutes(app);

// Both Routes
BothRoutes(app);

// Admin Routes
AdminRoutes(app);

GenerateToken(app);

const PORT = process.env.PORT || 5000;

// Start the Server
io.listen(
  app.listen(PORT, () => console.log(`Server started on port ${PORT}`))
);

CustomerSocket(app, io);
DriverSocket(app, io);
