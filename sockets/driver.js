const jwt = require('jsonwebtoken');
const config = require('config');
const mongoose = require('mongoose');
const User = require('../models/User');
const Request = require('../models/Request');

module.exports = function (app, io) {
  app.io = io
    .of('/driver')
    .use(async (socket, next) => {
      if (socket.handshake.query && socket.handshake.query.token) {
        const decoded = jwt.verify(
          socket.handshake.query.token,
          config.get('jwtToken')
        );

        let user = await User.findById(decoded.user.id, ['role']);

        if (user && user.role == 'driver') {
          socket.decoded = decoded;
          user.status = 'online';
          await user.save();
          // Join room with own Id (driver+id)
          socket.join('Driver' + decoded.user.id);

          next();
        }
      } else {
        next(new Error('Authentication error'));
      }
    })
    .on('connection', function (socket) {
      socket.emit('connect', 'You are connected');

      socket.on('disconnect', function () {
        User.findOneAndUpdate(
          { _id: socket.decoded.user.id, role: 'driver', status: 'online' },
          { $set: { status: 'online' } },
          function (err, updatedUser) {
            if (err) {
              console.log(
                'error on updating driver status when he disconnects'
              );
            } else {
              console.log('Got disconnected!');
            }
          }
        );
      });

//      socket.on('updateLocation', function (data) {
//        console.log('updatingLocation');
//        console.log(data);
//        const lastLocation = {
//          type: 'Point',
//          coordinates: [data.Coordinate.Longitude, data.Coordinate.Latitude],
//        };

//        User.findOneAndUpdate(
//          { _id: socket.decoded.user.id },
//          { $set: { lastLocation: lastLocation } },
//          function (err, updatedUser) {
//            if (err) {
//              socket.emit('locationResponse', err);
//           } else {
//              socket.emit('locationResponse', updatedUser);
//            }
//          }
//        );
//      });

      socket.on('sendQuoteOnRequest', async (data) => {
        const { price, ETA, vehicleMake, vehicleRegNumber } = data;
        const req = data.request;
        const driverId = data.driverId;
        const driverLocation = data.driverLocation;
        console.log(req);
	      
        let request = await Request.findById(req._id);
        const quotesOnRequest = request.quotes;

        if (quotesOnRequest.some((el) => el.driverId == driverId)) {
          socket.emit('response', 'Quote already placed');
        } else {
          const quoteObject = {};
          quoteObject._id = mongoose.Types.ObjectId();
          quoteObject.driverId = driverId;
          quoteObject.price = price;
          quoteObject.ETA = ETA;

          request.quotes.push(quoteObject);
          await request.save();
          quoteObject.requestId = request._id;
	        const driver = await User.findById(driverId).select('-password');
          socket.emit('response', {msg: 'Quote placed', quoteId: quoteObject._id});
          io.of('/customer')
            .to('Customer' + req.customer)
            .emit('quoteOnRequest', {quote:quoteObject, driver: driver, vehicleMake: vehicleMake, vehicleRegNumber: vehicleRegNumber, driverLocation: driverLocation});
	      }
      });

      socket.on('startRide', async (requestId) => {
        let request = await Request.findById(requestId).populate('driverId');
        request.status = 'InProgress';
        await request.save();
        socket.emit('response', 'Ride Started');
	      io.of('/customer')
          .to('Customer' + request.customer)
          .emit('rideStarted', request);
      });

      socket.on('endRide', async (data) => {
        const { requestId, pin } = data;
        let request = await Request.findById(requestId);
        if (request.pin != pin) {
          socket.emit('response', 'Invalid PIN');
        } else {
          request.status = 'Completed';
          await request.save();
	        await User.findByIdAndUpdate(request.driverId, {is_busy: false}, function(err) {
            if(err) return err.send('Server Error');
          });
          socket.emit('response', 'Ride Ended');
	        io.of('/customer')
            .to('Customer' + request.customer)
            .emit('rideEnded', 'Ride Ended');
        }
      });

      socket.on('deleteQuote', async (data) => {
        const requestId = data.requestId;
	      const quoteId = data.quoteId;

        if (requestId && quoteId) {
          let request = await Request.findById(requestId).select('-pin');
	        let foundQuote = await request.quotes.filter(
            (el) => el._id == quoteId
          );

	        // if (foundQuote.length >= 1) {
          try {
            await Request.update({ _id: requestId}, { "$pull": { "quotes": {"_id":quoteId }}}, function(err, quote) {
              if(err) socket.emit('response', {error: err}); 
              else {
                socket.emit('response', {msg: 'Quote Deleted', quoteId: quoteId});
                io.of('/customer')
                  .to('Customer' + request.customer)
                  .emit('quoteDeleted', {msg: 'Quote Deleted', quoteId: quoteId});
              }
            })
          } catch(err) {
            socket.emit('response', {error: err})
          }
        }
      });

      socket.on('cancelRequest', async (data) => {
        const requestId = data;

        if (requestId) {
          let request = await Request.findById(requestId, ['-pin']);
          let customer = request.customer;
          request.remove();
	  
          socket.emit('response', 'Request Cancelled');
          io.of('/customer')
            .to('Customer' + customer)
            .emit('requestDeleted', 'Job Cancelled By Driver');
        }
      });

      socket.on('cancelRide', async (data) => {
        const requestId = data;

        if (requestId) {
          let request = await Request.findById(requestId, ['-pin']);
          request.status = "CancelledByDriver";
          request.save();
          await User.findByIdAndUpdate(request.driverId, {is_busy: false}, function(err) {
            if(err) return err.send('Server Error');
          });
          io.of('/customer')
            .to('Customer' + request.customer)
            .emit('rideCancelled', 'Ride Cancelled by the Driver');
          socket.emit('response', 'Ride Cancelled');
        } else {
            socket.emit('response', 'Request Id Not Found');
        }
      });

      socket.on('updateDriverLocation', async (data) => {
        const latitude = data.latitude;
        const longitude = data.longitude;

        let driver = await User.findById(socket.decoded.user.id);
        driver.location = {
          type: "Point",
          coordinates: [longitude, latitude]
        };
        await driver.save();
        socket.emit('response', {message: 'location updated', lat: latitude, lon: longitude});
      });

      socket.on('updateLocation', async (data) => {
        const latitude = data.latitude;
        const longitude = data.longitude;
        const customerId = data.customerId;

	      let driver = await User.findById(socket.decoded.user.id);
        driver.location = {
          type: "Point",
          coordinates: [longitude, latitude]
        };
        await driver.save();
        socket.emit('response', {message: 'location updated', lat: latitude, lon: longitude, customer: customerId});
        io.of('/customer')
          .to('Customer' + customerId)
          .emit('receiveDriverLocation', {lat: latitude, lon: longitude});
      });

      socket.on('pickedupKey', async (requestId) => {
        let request = await Request.findById(requestId, ['-pin']);
        request.status = 'KeysPickedUp';
        await request.save();
        socket.emit('response', 'Keys pickedup, now head to towards car');
        io.of('/customer')
          .to('Customer' + request.customer)
          .emit('pickedupKey', 'driver has picked up key and heading towards car');
      });

      socket.on('vehiclePickedUp', async (requestId) => {
        let request = await Request.findById(requestId, ['-pin']);
        request.status = 'vehiclePickedUp';
        await request.save();
        socket.emit('response', 'vehicle pickedup, now head to towards drop off location');
        io.of('/customer')
          .to('Customer' + request.customer)
          .emit('vehiclePickedUp', 'driver has picked up vehicle and heading towards drop off location');
      });


      // socket.on('getOnline', function (data) {
      //   User.findOne({ _id: socket.decoded.user.id }, function (
      //     err,
      //     foundUser
      //   ) {
      //     if (err) {
      //       socket.emit('response', err);
      //     } else {
      //       // socket.emit('locationResponse', updatedUser);
      //       socket.join('Driver' + socket.decoded.user.id);
      //       socket.emit(
      //         'response',
      //         'Joined room: Driver' + socket.decoded.user.id
      //       );
      //       console.log(data);
      //       foundUser.status = 'online';
      //       foundUser.save();
      //     }
      //   });
      // });
    });
};
