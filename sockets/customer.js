const jwt = require('jsonwebtoken');
const config = require('config');
const User = require('../models/User');
const Request = require('../models/Request');
const geolib = require('geolib');
const randomString = require('randomstring');

module.exports = function (app, io) {
  app.io = io
    .of('/customer')
    .use(async (socket, next) => {
      if (socket.handshake.query && socket.handshake.query.token) {
        const decoded = jwt.verify(
          socket.handshake.query.token,
          config.get('jwtToken')
        );
        const user = await User.findById(decoded.user.id, ['role']);
        if (user && user.role == 'customer') {
          socket.decoded = decoded;

          // Join room with own id (customer+id)
          socket.join('Customer' + decoded.user.id);
          next();
        }
      } else {
        next(new Error('Authentication error'));
      }
    })
    .on('connection', function (socket) {
      socket.emit('connect', 'You are connected');

      socket.on('disconnect', function () {
        User.findOneAndUpdate(
          { _id: socket.decoded.user.id, role: 'driver', status: 'online' },
          { $set: { status: 'offline' } },
          function (err, updatedUser) {
            if (err) console.log('error on updating driver status when he disconnects');
            else console.log('Got disconnected!');
          }
        );
      });

      socket.on('updateLocation', function (data) {
        const lastLocation = {
          type: 'Point',
          coordinates: [data.Coordinate.Longitude, data.Coordinate.Latitude],
        };

        User.findOneAndUpdate(
          { _id: socket.decoded.user.id },
          { $set: { lastLocation: lastLocation } },
          function (err, updatedUser) {
            if (err) socket.emit('locationResponse', err);
            else socket.emit('locationResponse', updatedUser);
          }
        );
      });

      // socket.on('getOnline', function (data) {
      //   User.findOne({ _id: socket.decoded.user.id }, function (
      //     err,
      //     foundUser
      //   ) {
      //     if (err) {
      //       socket.emit('response', err);
      //     } else {
      //       // socket.emit('locationResponse', updatedUser);
      //       socket.join('Driver' + socket.decoded.user.id);
      //       socket.emit(
      //         'response',
      //         'Joined room: Driver' + socket.decoded.user.id
      //       );
      //       console.log(data);
      //       foundUser.status = 'online';
      //       foundUser.save();
      //     }
      //   });
      // });

      socket.on('acceptQuote', async (data) => {
        const { requestId, quoteId } = data;

        if (requestId && quoteId) {
          let request = await Request.findById(requestId, ['-pin']);
          let foundQuote = await request.quotes.filter(
            (el) => el._id == quoteId
          );
          if (foundQuote.length >= 1) {
            request.driverId = foundQuote[0].driverId;
            request.selectedQuoteId = quoteId;
            request.status = 'Assigned';
            await request.save();

	          let driver = await User.findById(foundQuote[0].driverId);
            driver.is_busy = true;
            await driver.save();

            socket.emit('response', {msg: 'Quote Accepted, Ride Assigned to Driver', driver: driver});
            const customer = await User.findById(request.customer).select('-password');
	          request.quotes.forEach(quote => {
              if(quote.driverId !== request.driverId) {
                io.of('/driver')
                  .to('Driver' + quote.driverId)
                  .emit('quoteAccepted', {request: null, msg:'Other Driver\'s quote has been accepted'});
              }
            });
            io.of('/driver')
              .to('Driver' + request.driverId)
              .emit('quoteAccepted', {request:request, customer: customer});
          }
        }
      });

      socket.on('breakdownRequest', function (data) {
        if (
          !('numberOfPassengers' in data) ||
          !('pickupLocation' in data) ||
          !('pickupCity' in data) ||
          !('pickupAddress' in data) ||
          !('dropoffLocation' in data) ||
          !('dropoffCity' in data) ||
          !('dropoffAddress' in data) ||
          !('breakdownType' in data) ||
          !('userLocation' in data) ||
	        !('vehicleId' in data)
        ) {
          console.log('error');
          socket.emit(
            'response',
            'Please send {numberOfPassengers: 4,pickupLocation: "2,4",pickupCity: "pickupCity",pickupAddress: "Address",dropoffLocation: "5,2",dropoffCity: "dropoffCity", userLocation:{longitude:"2",latitude:"5"},dropoffAddress: "Address",breakdownType: "breakdownType","vehicleId": vehicleId}'
          );
        } else {
          User.findById(socket.decoded.user.id, (err, foundUser) => {
            if (err) console.log(err);
            try {
              const pickupCoordinates = [
                data.pickupLocation.split(',')[1],
                data.pickupLocation.split(',')[0],
              ];
              const dropoffCoordinate = [
                data.dropoffLocation.split(',')[1],
                data.dropoffLocation.split(',')[0],
              ];
              const pickupLocation = {
                type: 'Point',
                coordinates: pickupCoordinates,
              };
              const dropoffLocation = {
                type: 'Point',
                coordinates: dropoffCoordinate,
              };

              let distance = geolib.getPreciseDistance(
                { latitude: pickupCoordinates[0], longitude: pickupCoordinates[1] },
                { latitude: dropoffCoordinate[0], longitude: dropoffCoordinate[1] }
              );
              const distancePickupToDropoff = parseFloat(distance)*parseFloat(0.000621);
              const pin = randomString.generate({
                length: 4,
                charset: 'numeric',
              });
              const customer = foundUser._id;
              const {
                numberOfPassengers,
                breakdownType,
                pickupAddress,
                pickupCity,
                dropoffAddress,
                dropoffCity,
                userLocation,
                notes,
                vehicleId,
                vehicleRegNumber,
                vehicleMake
              } = data;

              const requestType = 'breakdown';
              const pickup = {
                location: pickupLocation,
                address: pickupAddress,
                city: pickupCity,
              };
              const dropoff = {
                location: dropoffLocation,
                address: dropoffAddress,
                city: dropoffCity,
              };
              let request = new Request({
                breakdownType,
                requestType,
                numberOfPassengers,
                customer,
                dropoff: dropoff,
                pickup: pickup,
                distancePickupToDropoff,
                pin,
                notes,
                vehicleRegistration: vehicleRegNumber,
                vehicleMake: vehicleMake
              });

              request.save().then(() => {
                // Send request to all nearby Drivers for Quotes
                User.find(
                  {
                    role: 'driver',
                    status: 'online',
          		   // is_busy: false
                    location: {
	                    $near: {$maxDistance: 64374, $geometry: {type: "Point",coordinates: [userLocation[1], userLocation[0]]}}
                    }
                  },
                  function (err, nearbyDrivers) {
                    if (err) {
                      console.log(err);
                      socket.emit('response', err);
                    } else {
                      if (nearbyDrivers.length < 1) {
                        socket.emit(
                          'response',
                          'No drivers available right now.'
                        );
                      } else {
                        request.pin = undefined;
                        User.find({ _id: foundUser._id}, function(err, data) {
                          if (err) socket.emit('response', err);
                          else {
                            nearbyDrivers.forEach(function (user) {
                              io.of('/driver')
                                .to('Driver' + user._id)
                                .emit('breakdownRequest', {requestData: request, customer:data, vehicleId: vehicleId, vehicleRegNumber: vehicleRegNumber, vehicleMake: vehicleMake})
                            });
                          }
                        })
                        socket.emit('response', {numberOfDrivers:nearbyDrivers, requestData: request});
                      }
                    }
                  }
                );
              });
            } catch (error) {
              socket.emit('response', error.message);
            }
          });
        }
      });

      socket.on('relocateRequest', function (data) {
        if (
          !('pickupLocation' in data) ||
          !('pickupCity' in data) ||
          !('pickupAddress' in data) ||
          !('pickupKeysLocation' in data) ||
          !('pickupKeysCity' in data) ||
          !('pickupKeysAddress' in data) ||
          !('dropoffLocation' in data) ||
          !('dropoffCity' in data) ||
          !('dropoffAddress' in data) ||
          !('vehicleRegistration' in data) ||
          !('vehicleMake' in data)
        ) {
          console.log('error');
          socket.emit(
            'response',
            'Please send {pickupLocation: "2,4",pickupCity: "pickupCity",pickupAddress: "Address",dropoffLocation: "5,2",dropoffCity: "dropoffCity",dropoffAddress: "Address",pickupKeysLocation: "5,2",pickupKeysCity: "pickupKeysCity",pickupKeysAddress: "Address",vehicleRegistration: "vehicleRegistration", vehicleMake: "vehicleMake"}'
          );
        } else {
          User.findById(socket.decoded.user.id, (err, foundUser) => {
            if (err) console.log(err);
            try {
              const pickupCoordinates = [
                data.pickupLocation.split(',')[1],
                data.pickupLocation.split(',')[0],
              ];
              const dropoffCoordinate = [
                data.dropoffLocation.split(',')[1],
                data.dropoffLocation.split(',')[0],
              ];
              const pickupKeyCoordinates = [
                data.pickupKeysLocation.split(',')[1],
                data.pickupKeysLocation.split(',')[0],
              ];
              const pickupLocation = {
                type: 'Point',
                coordinates: pickupCoordinates,
              };
              const dropoffLocation = {
                type: 'Point',
                coordinates: dropoffCoordinate,
              };
              const pickupKeysLocation = {
                type: 'Point',
                coordinates: pickupKeyCoordinates,
              };
              let keysToPickupDistance = geolib.getPreciseDistance(
                { latitude: pickupKeyCoordinates[0], longitude: pickupKeyCoordinates[1] },
                { latitude: pickupCoordinates[0], longitude: pickupCoordinates[1] }
              );
              const distanceKeysToPickup = parseFloat(keysToPickupDistance)*parseFloat(0.000621);
              let pickupToDropoffDistance = geolib.getPreciseDistance(
                { latitude: pickupCoordinates[0], longitude: pickupCoordinates[1] },
                { latitude: dropoffCoordinate[0], longitude: dropoffCoordinate[1] }
              );
              const distancePickupToDropoff = parseFloat(pickupToDropoffDistance)*parseFloat(0.000621);
              const pin = randomString.generate({
                length: 4,
                charset: 'numeric',
              });
              const customer = foundUser._id;
              const {
                pickupAddress,
                pickupCity,
                pickupKeysAddress,
                pickupKeysCity,
                dropoffAddress,
                dropoffCity,
                userLocation,
                notes,
                vehicleRegistration,
                vehicleMake,
		            isKeyAtPickupLocation
              } = data;

              const requestType = 'relocate';
              const pickup = {
                location: pickupLocation,
                address: pickupAddress,
                city: pickupCity,
              };
              const pickupKeys = {
                location: pickupKeysLocation,
                address: pickupKeysAddress,
                city: pickupKeysCity,
              };
              const dropoff = {
                location: dropoffLocation,
                address: dropoffAddress,
                city: dropoffCity,
              };
              let request = new Request({
                requestType,
                customer,
                dropoff: dropoff,
                pickup: pickup,
                pickupKeys: pickupKeys,
                pin,
                notes,
                distanceKeysToPickup,
                distancePickupToDropoff,
                vehicleRegistration,
                vehicleMake,
		            isKeyAtPickupLocation
              });

              request.save().then(() => {
                // Send request to all nearby Drivers for Quotes
                User.find(
                  {
                    role: 'driver',
                    status: 'online',
                //  is_busy: false
                //    lastLocation: {
               //       $near: {
              //          $geometry: {
               //           type: 'Point',
                //          coordinates: [
                 //           parseFloat(pickupKeyCoordinates[0]),
                  //          parseFloat(pickupKeyCoordinates[1]),
                   //       ],
                    //    },
                     //   $maxDistance: 1000,
                 //     },
                //    },
                    location: {
                      $near: {$maxDistance: 64374, $geometry: {type: "Point",coordinates: [userLocation[1], userLocation[0]]}}
                    }
                  },
                  function (err, nearbyDrivers) {
                    if (err) socket.emit('response', {err: err});
                    else {
                      if (nearbyDrivers.length < 1) socket.emit('response', 'No drivers available right now.');
                      else {
			                  User.find({ _id: foundUser._id}, function(err, data) {
                          socket.emit('response', {numberOfDrivers: nearbyDrivers.length, request: request});
                          nearbyDrivers.forEach(function (user) {
                            io.of('/driver')
                              .to('Driver' + user._id)
                              .emit('relocateRequest', {request: request, customer:data});
                          });
                          socket.emit('response', 'Waiting for drivers to respond');
			                  });
                      }
                    }
                  }
                );
              });
            } catch (error) {
              socket.emit('response', error.message);
            }
          });
        }
      });
      
      socket.on('cancelRequest', async (data) => {
        const requestId = data;
        if (requestId) {
          let request = await Request.findById(requestId, ['-pin']);
          User.find({
            role: 'driver',
            status: 'online',
            is_busy: false
            // lastLocation: {
            //   $near: {
            //     $geometry: {
            //       type: 'Point',
            //       coordinates: [
            //         parseFloat(userLocation.longitude),
            //         parseFloat(userLocation.latitude),
            //       ],
            //     },
            //     $maxDistance: 10000000,
            //   },
            // },
          },
          function (err, nearbyDrivers) {
            if (err) socket.emit('response', err);
            else {
              if (nearbyDrivers.length < 1) socket.emit('response', 'No drivers available right now.');
              else {
                request.pin = undefined;
                nearbyDrivers.forEach(function (user) {
                  io.of('/driver')
                    .to('Driver' + user._id)
                    .emit('requestDeleted', 'Request Deleted By The User');
                });
              }
            }
          });

          request.delete();
          socket.emit('response', 'Request Deleted');
        } else socket.emit('response', 'Request Id Not Found');
      });

      socket.on('rejectQuote', async (data) => {
        const requestId = data.requestId;
        const quoteId = data.quoteId;

        if (requestId && quoteId) {
          let request = await Request.findById(requestId).select('-pin');
          let foundQuote = await request.quotes.filter(
            (el) => el._id == quoteId
          );

         // if (foundQuote.length >= 1) {
          try {
            await Request.update({ _id: requestId}, { "$pull": { "quotes": {"_id":quoteId }}}, function(err, quote) {
              if(err) socket.emit('response', {error: err});
              else {
                socket.emit('response', {msg: 'Quote rejected', quoteId: quoteId});
                io.of('/driver')
                  .to('Driver' + foundQuote[0].driverId)
                  .emit('quoteRejected', {msg: 'Your quote is rejected by the customer', quoteId: quoteId});
              }
            })
          } catch(err) {
              socket.emit('response', {error: err})
          }
        }
      });

      socket.on('cancelRide', async (data) => {
        const requestId = data;
        if (requestId) {
          let request = await Request.findById(requestId, ['-pin']);
          request.status = "CancelledByCustomer";
          request.save();
          await User.findByIdAndUpdate(request.driverId, {is_busy: false}, function(err) {
            if(err) return err.send('Server Error');
          });
          io.of('/driver')
            .to('Driver' + request.driverId)
            .emit('rideCancelled', 'Ride Cancelled by the customer');
          socket.emit('response', 'Ride Cancelled');
        } else socket.emit('response', 'Request Id Not Found');
      });


      // socket.on('onlineDrivers', (data) => {
      //   const drivers = io.of('/driver').sockets.adapter;
      //   console.log(JSON.stringify(drivers));
      // });

      // socket.on('joinRideRoom', function (data) {
      //   if (!data.request.id) {
      //     Request.findOne(
      //       { _id: data.request.id, driverId: socket.decoded.user.id },
      //       function (err, foundRide) {
      //         if (err) {
      //           socket.emit('response', err);
      //         } else {
      //           // socket.emit('locationResponse', updatedUser);
      //           if (!foundRide) {
      //             socket.emit('response', 'No Ride found');
      //           }
      //           socket.join('Request' + data.request.id);
      //           socket.emit(
      //             'response',
      //             'Joined room: Request' + data.request.id
      //           );
      //         }
      //       }
      //     );
      //   } else {
      //     socket.emit('response', 'Please send Request: {id: "somerequestID"}');
      //   }
      // });
    });
};
